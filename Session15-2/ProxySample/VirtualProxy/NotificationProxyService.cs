﻿using System;
using System.ComponentModel;

namespace ProxySample.VirtualProxy
{
    public class NotificationProxyService : INotificationService
    {
        private readonly Lazy<INotificationService> _notificationService;
        public NotificationProxyService(Lazy<INotificationService> notificationService)
        {
            _notificationService = notificationService;
        }
        public void SendWelcome(string username)
        {
            _notificationService.Value.SendWelcome(username);
            //if (_subject == null)
            //    _subject = new ExpensiveNotificationService();
            //_subject.SendWelcome(username);
        }
    }

    //public interface INotificationServiceFactory{}
    //public class AutofacNotificationServiceFactory : INotificationServiceFactory
    //{
    //    public AutofacNotificationServiceFactory(IContainer )
    //    {
            
    //    }
      
    //}
}