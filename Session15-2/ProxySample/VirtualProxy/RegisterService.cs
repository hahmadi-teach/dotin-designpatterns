﻿namespace ProxySample.VirtualProxy
{
    public class RegisterService
    {
        private readonly INotificationService _notificationService;
        public RegisterService(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        public void RegisterUser(string username)
        {
            //....register user
            // if (registrationWasSuccessful)
                    _notificationService.SendWelcome(username);
        }
    }
}