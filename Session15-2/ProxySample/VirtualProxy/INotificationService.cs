﻿namespace ProxySample.VirtualProxy
{
    public interface INotificationService
    {
        void SendWelcome(string username);
    }
}