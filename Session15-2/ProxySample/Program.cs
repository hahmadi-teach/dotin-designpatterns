﻿using System;
using ProxySample.VirtualProxy;

namespace ProxySample
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new Lazy<ExpensiveNotificationService>();
            service.Value.SendWelcome("admin");

            Console.WriteLine("Hello World!");
        }
    }
}
