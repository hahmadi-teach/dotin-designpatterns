﻿namespace CommandSample
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}