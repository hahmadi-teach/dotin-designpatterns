﻿namespace CommandSample
{
    public class TurnOnCommand : ICommand
    {
        private readonly Lamp _lamp;
        public TurnOnCommand(Lamp lamp)
        {
            _lamp = lamp;
        }
        public void Execute()
        {
            this._lamp.TurnOn();
        }

        public void Undo()
        {
            this._lamp.TurnOff();
        }
    }
}