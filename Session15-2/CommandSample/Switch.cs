﻿using System.Collections.Generic;
using System.Linq;

namespace CommandSample
{
    public class Switch
    {
        private Stack<ICommand> _history = new Stack<ICommand>();
        public void Perform(ICommand command)
        {
            command.Execute();
            _history.Push(command);
        }

        public void Undo()
        {
            if (!_history.Any()) return;
            var latestCommand = _history.Pop();
            latestCommand.Undo();
        }
    }
}