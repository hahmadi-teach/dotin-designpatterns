﻿using System;

namespace CommandSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var lamp = new Lamp();
            var turnOnCommand = new TurnOnCommand(lamp);

            var lampSwitch = new Switch();          //Invoker
            lampSwitch.Perform(turnOnCommand);

            Console.WriteLine("Hello World!");
        }
    }
}
