﻿namespace CommandSample
{
    public class Lamp
    {
        public bool IsOn { get; private set; }
        public void TurnOff()
        {
            this.IsOn = false;
        }
        public void TurnOn()
        {
            this.IsOn = true;
        }
    }
}