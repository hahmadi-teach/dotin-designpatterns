﻿namespace CommandSample
{
    public class TurnOffCommand : ICommand
    {
        private readonly Lamp _lamp;
        public TurnOffCommand(Lamp lamp)
        {
            _lamp = lamp;
        }
        public void Execute()
        {
            this._lamp.TurnOff();
        }

        public void Undo()
        {
            this._lamp.TurnOn();
        }
    }
}