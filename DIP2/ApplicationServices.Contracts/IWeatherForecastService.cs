﻿using System.Collections.Generic;

namespace ApplicationServices
{
    public interface IWeatherForecastService
    {
        List<WeatherForecast> GetForecastSummaries();
    }
}