﻿namespace SingletonSample
{
    public class LoggerCSharpWay
    {
        public static LoggerCSharpWay Current { get; private set; }  = new LoggerCSharpWay();
        private LoggerCSharpWay() { }
        public void Write(string message)
        {

        }
    }

}