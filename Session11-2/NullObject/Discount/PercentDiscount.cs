﻿namespace NullObject.Discount
{
    public class PercentDiscount : IDiscountStrategy
    {
        private readonly decimal _percent;
        public PercentDiscount(decimal percent)
        {
            _percent = percent;
        }
        public decimal AmountToDiscount(decimal price)
        {
            return _percent * price / 100;
        }
    }

    public class VipDiscount : IDiscountStrategy
    {
        public decimal AmountToDiscount(decimal price)
        {
            return price / 2;
        }
    }

    public class NoDiscount : IDiscountStrategy
    {
        public decimal AmountToDiscount(decimal price)
        {
            return 0;
        }
    }
}