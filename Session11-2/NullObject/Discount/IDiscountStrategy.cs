﻿namespace NullObject.Discount
{
    public interface IDiscountStrategy
    {
        decimal AmountToDiscount(decimal price);
    }
}