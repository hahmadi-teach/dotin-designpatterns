﻿using System;
using PosTcp.Framework;
using PosTcp.Handlers;
using PosTcp.Model;

namespace PosTcp
{
    class Program
    {
        static void Main(string[] args)
        {
            var packet = new TcpPacket { Data = "HEARTBEAT" };
            var chain = TcpChainFactory.Create();
            chain.Handle(packet);

            Console.ReadLine();
        }
    }
}
