﻿using System;
using PosTcp.Framework;
using PosTcp.Model;

namespace PosTcp.Handlers
{
    public class PaymentHandler : Handler<TcpPacket>
    {

        protected override bool CanHandle(TcpPacket input)
        {
            return input.Data == "PAYMENT";
        }

        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of payment");
        }
    }
}