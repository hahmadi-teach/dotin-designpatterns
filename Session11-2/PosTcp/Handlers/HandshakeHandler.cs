﻿using System;
using PosTcp.Framework;
using PosTcp.Model;

namespace PosTcp.Handlers
{
    public class HandshakeHandler : Handler<TcpPacket>
    {
        private bool _isAuthenticated;
        protected override bool CanHandle(TcpPacket input)
        {
            return input.Data == "HANDSHAKE";
        }
        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of handshake");
        }
        protected override void BeforeCallingTheSuccessor(TcpPacket input)
        {
            if (!_isAuthenticated) throw new UnauthorizedAccessException();
        }
    }
}