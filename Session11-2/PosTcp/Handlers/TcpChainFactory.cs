﻿using PosTcp.Framework;
using PosTcp.Model;

namespace PosTcp.Handlers
{
    public static class TcpChainFactory
    {
        public static IHandler<TcpPacket> Create()
        {
            return new ChainBuilder<TcpPacket>()
                .Append<HeartbeatHandler>()
                .Append<HandshakeHandler>()
                .Append<PaymentHandler>()
                .Append<PrintHandler>()
                .AppendEndOfChainHandler()
                .Build();
        }
    }
}