﻿using System;
using PosTcp.Framework;
using PosTcp.Model;

namespace PosTcp.Handlers
{
    public class PrintHandler : Handler<TcpPacket>
    {
        protected override bool CanHandle(TcpPacket input)
        {
            return input.Data == "Print";
        }
        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of print");
        }
    }
}