﻿using System;
using PosTcp.Framework;
using PosTcp.Model;

namespace PosTcp.Handlers
{
    public class HeartbeatHandler : Handler<TcpPacket>
    {
        protected override bool CanHandle(TcpPacket input)
        {
            return input.Data == "HEARTBEAT";
        }

        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of heartbeat");
        }
    }
}