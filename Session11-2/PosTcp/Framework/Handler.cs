﻿namespace PosTcp.Framework
{
    public abstract class Handler<T> : IHandler<T>
    {
        protected IHandler<T> Successor;
        public void Handle(T input)
        {
            if (CanHandle(input))
                DoHandle(input);
            else
            {
                BeforeCallingTheSuccessor(input);
                CallNext(input);
            }
        }

        protected abstract void DoHandle(T input);
        protected virtual bool CanHandle(T input)
        {
            return true;
        }
        public virtual void SetSuccessor(IHandler<T> successor)
        {
            this.Successor = successor;
        }

        public virtual void CallNext(T input)
        {
            this.Successor.Handle(input);
        }
        protected virtual void BeforeCallingTheSuccessor(T input) { }

    }
}