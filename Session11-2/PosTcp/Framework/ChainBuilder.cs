﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PosTcp.Framework
{
    public class ChainBuilder<T>
    {
        private List<IHandler<T>> _handlers = new List<IHandler<T>>();
        public ChainBuilder<T> Append(IHandler<T> handler)
        {
            _handlers.Add(handler);
            return this;
        }
        public ChainBuilder<T> Append<THandler>() where THandler : IHandler<T>, new()
        {
            return Append(new THandler());
        }
        public ChainBuilder<T> AppendEndOfChainHandler()
        {
            return Append(new EndOfChainHandler<T>());
        }
        public IHandler<T> Build()
        {
            _handlers.Aggregate((a, b) =>
            {
                a.SetSuccessor(b);
                return b;
            });
            return _handlers.First();
        }
    }
}
