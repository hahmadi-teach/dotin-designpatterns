﻿namespace BusinessLogic
{
    public interface ITaxRepository
    {
        long GetCurrentTaxRate();
    }
}