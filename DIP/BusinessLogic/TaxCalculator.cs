﻿using System;

namespace BusinessLogic
{
    public class TaxCalculator
    {
        private readonly ITaxRepository _repository;
        public TaxCalculator(ITaxRepository repository)
        {
            _repository = repository;
        }

        public long CalculateSalary(long baseSalary)
        {
            var taxRate = _repository.GetCurrentTaxRate();

            //...
            return 0;
        }
    }
}
