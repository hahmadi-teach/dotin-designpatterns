﻿namespace AssetManagement.Model.Visitors
{
    public class NetWorthCalculator : IVisitor
    {
        private int _netWorth;
        public void Visit(Car car)
        {
            _netWorth += car.Price;
        }

        public void Visit(RealEstate realEstate)
        {
            _netWorth += realEstate.Price;
        }

        public void Visit(BankAccount account)
        {
            _netWorth += account.Balance;
        }
        public int GetNetWorth() => _netWorth;
    }
}