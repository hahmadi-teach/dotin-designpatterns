﻿namespace AssetManagement.Model.Visitors
{
    public class MonthlyIncomeCalculator : IVisitor
    {
        private int _monthlyIncome;
        public void Visit(Car car)
        {
            _monthlyIncome -= car.MonthlyCost;
        }
        public void Visit(RealEstate realEstate)
        {
            _monthlyIncome += realEstate.MonthlyRentIncome;
            _monthlyIncome -= realEstate.MonthlyTax;
        }
        public void Visit(BankAccount account)
        {
            _monthlyIncome += account.MonthlyInterest;
        }
        public int GetMonthlyIncome() => _monthlyIncome;
    }
}