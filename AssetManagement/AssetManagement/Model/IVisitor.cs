﻿namespace AssetManagement.Model
{
    public interface IVisitor
    {
        void Visit(Car car);
        void Visit(RealEstate realEstate);
        void Visit(BankAccount account);
    }
}