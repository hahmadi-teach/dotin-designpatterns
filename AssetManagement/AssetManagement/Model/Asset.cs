﻿namespace AssetManagement.Model
{
    public abstract class Asset
    {
        public abstract void AcceptVisitor(IVisitor visitor);
    }
}