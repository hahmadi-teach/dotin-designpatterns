﻿using System.Collections.Generic;

namespace AssetManagement.Model
{
    public class Person
    {
        public List<Asset> Assets { get; set; }
        public void AcceptVisitor(IVisitor visitor)
        {
            foreach (var asset in Assets)
                asset.AcceptVisitor(visitor);
        }
    }
}
