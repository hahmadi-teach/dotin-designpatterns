﻿namespace AssetManagement.Model
{
    public class Car : Asset
    {
        public int MonthlyCost { get; set; }
        public int Price { get; set; }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}