﻿namespace AssetManagement.Model
{
    public class BankAccount : Asset
    {
        public int Balance { get; set; }
        public int MonthlyInterest { get; set; }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}