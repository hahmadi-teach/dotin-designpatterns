using System;
using System.Collections.Generic;
using AssetManagement.Model;
using AssetManagement.Model.Visitors;
using FluentAssertions;
using Xunit;

namespace AssetManagement.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var person = new Person()
            {
                Assets = new List<Asset>()
                {
                    new RealEstate()
                    {
                        Price = 100000000,
                        MonthlyTax = 200000,
                        MonthlyRentIncome = 1500000
                    },
                    new BankAccount()
                    {
                        Balance = 500000000,
                        MonthlyInterest = 200000
                    },
                    new Car()
                    {
                        Price = 10000000,
                        MonthlyCost = 300000
                    }
                }
            };
            var calculator = new MonthlyIncomeCalculator();
            person.AcceptVisitor(calculator);

            var result = calculator.GetMonthlyIncome();

            result.Should().Be(1200000);
        }


        [Fact]
        public void Test2()
        {
            var person = new Person()
            {
                Assets = new List<Asset>()
                {
                    new RealEstate()
                    {
                        Price = 100000000,
                        MonthlyTax = 200000,
                        MonthlyRentIncome = 1500000
                    },
                    new BankAccount()
                    {
                        Balance = 500000000,
                        MonthlyInterest = 200000
                    },
                    new Car()
                    {
                        Price = 10000000,
                        MonthlyCost = 300000
                    }
                }
            };
            var calculator = new NetWorthCalculator();
            person.AcceptVisitor(calculator);

            var result = calculator.GetNetWorth();

            result.Should().Be(610000000);
        }
    }
}
