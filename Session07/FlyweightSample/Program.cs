﻿using System;

namespace FlyweightSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var name1 = "Jack";
            var name2 = string.Intern(Console.ReadLine());

            Console.WriteLine(ReferenceEquals(name1, name2));

            Console.ReadLine();
        }
    }
}
