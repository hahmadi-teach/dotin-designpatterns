﻿using System;
using System.IO;

namespace Compression
{
    public class FolderCompressor
    {
        private readonly ICompressor _compressor;
        public FolderCompressor(ICompressor compressor)
        {
            this._compressor = compressor;
        }

        public void Compress(string folderPath)
        {
            if (!Directory.Exists(folderPath)) throw new DirectoryNotFoundException();
            var files = Directory.GetFiles(folderPath);
            foreach (var file in files)
            {
                var content = File.ReadAllBytes(file);
                var compressed = _compressor.Compress(content);  //Context passing data to strategy
                //.....
            }
        }
    }
}
