﻿namespace Compression
{
    public interface ICompressor
    {
        byte[] Compress(byte[] fileContent);
    }

    public class ZipCompressor : ICompressor
    {
        public byte[] Compress(byte[] fileContent)
        {
            return null;
        }
    }

    public class RarCompressor : ICompressor
    {
        public byte[] Compress(byte[] fileContent)
        {
            return null;
        }
    }
}