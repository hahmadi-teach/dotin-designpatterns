﻿using System;

namespace StateSample
{
    public abstract class OrderState
    {
        public virtual bool CanUpdate() => false;
        public virtual bool CanShip() => false;
        public virtual OrderState GoToShippedState() => throw new NotSupportedException();
    }
    public class DraftState : OrderState
    {
        public override bool CanUpdate()
        {
            return true;
        }
    }

    public class ConfirmedState : OrderState
    {
        public override bool CanShip()
        {
            return true;
        }
        public override OrderState GoToShippedState()=> new ShippedState();
    }
    public class ShippedState : OrderState
    {
    }
}