﻿using System;

namespace StateSample
{
    public class Order
    {
        public OrderState State { get; private set; }
        public Order()
        {
            this.State = new DraftState();    
        }
        public void Update()
        {
            if (!this.State.CanUpdate()) throw new NotSupportedException();
            //.....
        }
        public void Ship()
        {
            //this.State = this.State.GoToShippedState();

            //if (this.State.CanShip())
            //{
            //    //this.State = new ShippedState();
            //    //this.State = this.State.GoToNextStep();
            //    this.State = this.State.GoToShippedState();
            //}
        }
    }
}
