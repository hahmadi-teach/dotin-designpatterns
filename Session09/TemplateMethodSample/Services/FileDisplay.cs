﻿using System;
using System.Collections.Generic;

namespace TemplateMethodSample.Services
{
    public abstract class FileDisplay
    {
        public void Display(string path)
        {
            GuardAgainstInvalidFile(path);
            var content = ReadContentOfFile(path);
            content = ChangeContentAfterRead(content);
            var itemsToDisplay = Parse(content);        // ?
            DisplayItems(itemsToDisplay);
        }

        protected virtual string ChangeContentAfterRead(string content)
        {
            return content;
        }

        private void GuardAgainstInvalidFile(string path)
        {
            
        }
        private string ReadContentOfFile(string path)
        {
            throw new NotImplementedException();
        }
        protected abstract Dictionary<string, string> Parse(string content);
        private void DisplayItems(Dictionary<string, string> itemsToDisplay)
        {
        }
    }
}