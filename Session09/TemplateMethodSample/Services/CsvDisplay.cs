﻿using System.Collections.Generic;

namespace TemplateMethodSample.Services
{
    public class CsvDisplay : FileDisplay
    {
        protected override Dictionary<string, string> Parse(string content)
        {
            return new Dictionary<string, string>();
        }
    }

    public class XmlDisplay : FileDisplay
    {
        protected override Dictionary<string, string> Parse(string content)
        {
            return new Dictionary<string, string>();
        }
    }
}