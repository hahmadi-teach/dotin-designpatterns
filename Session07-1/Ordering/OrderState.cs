﻿using System;

namespace Ordering
{
    public abstract class OrderState
    {
        public abstract bool CanUpdate();

        public abstract OrderState GoToConfirmed();
    }
    public class Draft : OrderState
    {
        public override bool CanUpdate() => true;
        public override OrderState GoToConfirmed()
        {
            return OrderStateFactory.Create<Confirmed>();
        }
    }
    public class Confirmed : OrderState
    {
        public override bool CanUpdate() => false;
        public override OrderState GoToConfirmed()
        {
            throw new NotSupportedException();
        }
    }

    public class Shipped : OrderState
    {
        public override bool CanUpdate() => false;
        public override OrderState GoToConfirmed()
        {
            throw new NotSupportedException();
        }
    }
}