﻿using System.Collections.Concurrent;

namespace Ordering
{
    public static class OrderStateFactory
    {
        public static ConcurrentDictionary<string, OrderState> States = new ConcurrentDictionary<string, OrderState>();
        public static T Create<T>() where T : OrderState, new()
        {
            var name = typeof(T).Name;
            return States.GetOrAdd(name, new T()) as T;
        }
    }
}