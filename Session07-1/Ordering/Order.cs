﻿using System;

namespace Ordering
{
    public class Order
    {
        public long Id { get; private set; }
        public DateTime CreateDate { get; private set; }
        public string Address { get; private set; }
        public OrderState State { get; private set; }

        public Order()
        {
            this.State = OrderStateFactory.Create<Draft>();
        }

        public void ChangeAddress(string address)
        {
            if (this.State.CanUpdate())
                this.Address = address;
        }
        public void Confirm()
        {
            //if (this.state.CanConfirm()? 
            // this.state = new Confirmed(); 
            this.State = this.State.GoToConfirmed();
        }
    }
}
