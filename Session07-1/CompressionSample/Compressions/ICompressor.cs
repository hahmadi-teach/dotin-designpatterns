﻿namespace CompressionSample.Compressions
{
    public interface ICompressor            //Strategy
    {
        byte[] Compress(byte[] content);
    }

    public class ZipCompressor : ICompressor            //Concrete Strategy
    {
        public byte[] Compress(byte[] content)
        {
            return null;
        }
    }
}