﻿using System.Collections.Generic;
using System.IO;
using CompressionSample.Compressions;

namespace CompressionSample
{
    public class FolderCompressor           //Context
    {
        private readonly ICompressor _compressor;
        public FolderCompressor(ICompressor compressor)
        {
            _compressor = compressor;
        }
        public byte[] Compress(string path)
        {
            var files = GetFiles(path);
            foreach (var file in files)
            {
                var content = ReadFile(files);
                var compressed = _compressor.Compress(content);     //Context passes data to strategies
                //_compress.Compress(this);
            }
            return null;
        }

        private byte[] ReadFile(List<string> files)
        {
            throw new System.NotImplementedException();
        }

        private List<string> GetFiles(string path)
        {
            throw new System.NotImplementedException();
        }
    }
}