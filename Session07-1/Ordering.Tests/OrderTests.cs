using System;
using FluentAssertions;
using Xunit;

namespace Ordering.Tests
{
    public class OrderTests
    {
        [Fact]
        public void order_is_draft_on_start()
        {
            var order = new Order();

            order.State.Should().BeOfType<Draft>();
        }

        [Fact]
        public void can_change_address_when_order_is_draft()
        {
            var order = new Order();
            const string newAddress = "test";

            order.ChangeAddress(newAddress);

            order.Address.Should().Be(newAddress);
        }

        [Fact]
        public void cant_change_address_when_order_is_confirmed()
        {
            var order = new Order();
            const string newAddress = "test";
            order.Confirm();

            order.ChangeAddress(newAddress);

            order.Address.Should().NotBe(newAddress);
        }
    }
}
