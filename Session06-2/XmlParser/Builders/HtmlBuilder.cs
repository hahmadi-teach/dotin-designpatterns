﻿using System.Collections.Generic;
using System.Text;
using XmlParser.Documents;

namespace XmlParser.Builders
{
    public class HtmlBuilder : IDocumentBuilder, IContentBuilder
    {
        private StringBuilder _stringBuilder;
        public HtmlBuilder()
        {
            this._stringBuilder = new StringBuilder();    
        }

        public IDocumentBuilder AddHeader(string text)
        {
            _stringBuilder.Append($"<html><title>{text}</title>");
            return this;
        }

        public IContentBuilder AddBody()
        {
            _stringBuilder.Append("<body>");
            return this;
        }

        public IContentBuilder AddFooter()
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddParagraph(string text)
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddBulletList(List<string> items)
        {
            _stringBuilder.Append("<ul>");
            foreach (var item in items)
                _stringBuilder.Append($"<li>{item}</li>");
            _stringBuilder.Append("</ul>");
            return this;
        }

        public IContentBuilder AddImage(string path)
        {
            throw new System.NotImplementedException();
        }

        public IDocument Build()
        {
            return new HtmlDocument();
        }
    }
}