﻿namespace XmlParser.Builders
{
    public interface IDocumentBuilder
    {
        IDocumentBuilder AddHeader(string text);
        IContentBuilder AddBody();
        IContentBuilder AddFooter();
    }
}