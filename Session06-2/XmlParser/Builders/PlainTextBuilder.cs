﻿using System.Collections.Generic;
using XmlParser.Documents;

namespace XmlParser.Builders
{
    public class PlainTextBuilder : IDocumentBuilder, IContentBuilder
    {
        public IDocumentBuilder AddHeader(string text)
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddBody()
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddFooter()
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddParagraph(string text)
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddBulletList(List<string> items)
        {
            throw new System.NotImplementedException();
        }

        public IContentBuilder AddImage(string path)
        {
            throw new System.NotImplementedException();
        }

        public IDocument Build()
        {
            return new PlainTextDocument();
        }
    }
}