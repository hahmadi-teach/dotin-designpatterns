﻿using System.Collections.Generic;
using XmlParser.Documents;

namespace XmlParser.Builders
{
    public interface IContentBuilder
    {
        IContentBuilder AddParagraph(string text);
        IContentBuilder AddBulletList(List<string> items);
        IContentBuilder AddImage(string path);
        IDocument Build();
    }
}