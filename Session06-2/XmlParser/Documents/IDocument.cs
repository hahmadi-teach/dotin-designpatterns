﻿namespace XmlParser.Documents
{
    public interface IDocument
    {
        void Save(string path);
    }
    public class HtmlDocument : IDocument
    {
        public void Save(string path) {  }
    }
    public class PlainTextDocument : IDocument
    {
        public void Save(string path)  { }
    }
}