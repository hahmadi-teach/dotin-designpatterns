﻿using System;
using XmlParser.Builders;
using XmlParser.Documents;

namespace XmlParser
{
    public class DocumentParser         //Director
    {
        private readonly IDocumentBuilder _builder;
        public DocumentParser(IDocumentBuilder builder)
        {
            _builder = builder;
        }

        public IDocument Parse(string xml)
        {
            //parse xml...
            var currentToken = "";

            if (currentToken == "Header")
            {
                var headerContent = GetHeaderText();
                _builder.AddHeader(headerContent);
            }

            return _builder.AddBody()
                .AddBulletList(null)
                .Build();
        }

        private string GetHeaderText()
        {
            return "";
        }
    }
}
