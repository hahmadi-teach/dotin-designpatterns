using System;
using System.Collections.Generic;
using FluentAssertions;
using IteratorSample.Framework;
using Xunit;

namespace IteratorSample.Tests
{
    public class EvenNumbersRangeTests
    {
        [Fact]
        public void Test1()
        {
            IDataCollection<int> range = new EvenNumbersRange(1, 10);
            var iterator = range.GetIterator();
            var expected = new List<int>(){2,4,6,8,10};

            var actual = new List<int>();
            while (iterator.HasNext())
                actual.Add(iterator.Next());

            actual.Should().BeEquivalentTo(expected);
        }
    }
}
