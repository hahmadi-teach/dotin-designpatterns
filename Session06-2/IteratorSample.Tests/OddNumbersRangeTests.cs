﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using IteratorSample.NEW;
using Xunit;

namespace IteratorSample.Tests
{
    public class OddNumbersRangeTests
    {
        [Fact]
        public void iterate_over_odd_numbers()
        {
            var range = new OddNumbersRange(1, 9);
            var expected = new List<int>(){1,3,5,7,9};

            var actual = range.ToList();

            actual.Should().BeEquivalentTo(expected);
        }
    }
}