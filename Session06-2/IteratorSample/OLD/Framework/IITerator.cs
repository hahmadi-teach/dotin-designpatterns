﻿namespace IteratorSample.Framework
{
    public interface IDataCollection<out T>
    {
        IIterator<T> GetIterator();
    }
    
    public interface IIterator<out T>
    {
        bool HasNext();
        T Next();
    }
}