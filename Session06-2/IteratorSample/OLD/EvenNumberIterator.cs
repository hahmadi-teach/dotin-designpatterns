﻿using IteratorSample.Framework;

namespace IteratorSample
{
    internal class EvenNumberIterator : IIterator<int>
    {
        private readonly EvenNumbersRange _range;
        private int _current;
        private int _end;
        public EvenNumberIterator(EvenNumbersRange range)
        {
            _range = range;

            if (_range.Start % 2 == 0)
                _current = _range.Start;
            else
                _current = _range.Start + 1;

            _end = _range.End;
        }
        public bool HasNext()
        {
            return _current <= _range.End;
        }
        public int Next()
        {
            var output = _current;
            _current += 2;
            return output;
        }
    }
}