﻿using System;
using IteratorSample.Framework;

namespace IteratorSample
{
    public class EvenNumbersRange : IDataCollection<int>
    {
        public int Start { get; private set; }
        public int End { get; private set; }
        public EvenNumbersRange(int start, int end)
        {
            Start = start;
            End = end;
        }
        public IIterator<int> GetIterator()
        {
            return new EvenNumberIterator(this);
        }
    }
}
