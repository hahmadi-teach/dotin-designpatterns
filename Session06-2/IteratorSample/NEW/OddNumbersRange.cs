﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IteratorSample.NEW
{
    public class OddNumbersRange : IEnumerable<int>
    {
        public int Start { get; private set; }
        public int End { get; private set; }
        public OddNumbersRange(int start, int end)
        {
            Start = start;
            End = end;
        }
        public IEnumerator<int> GetEnumerator()
        {
            return new OddNumberIterator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
