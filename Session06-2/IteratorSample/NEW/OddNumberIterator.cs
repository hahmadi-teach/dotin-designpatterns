﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace IteratorSample.NEW
{
    public class OddNumberIterator : IEnumerator<int>
    {
        private readonly OddNumbersRange _range;
        private int _current;
        public OddNumberIterator(OddNumbersRange range)
        {
            _range = range;
            Reset();
        }
        public bool MoveNext()
        {
            if ((_current + 2) > _range.End) 
                return false;
            else
            {
                _current += 2;
                return true;
            }
        }
        public void Reset()
        {
            if (_range.Start % 2 == 0)
                _current = _range.Start + 1;
            else
                _current = _range.Start - 2;
        }

        public int Current => _current;
        object IEnumerator.Current => Current;
        public void Dispose()
        {
        }
    }
}