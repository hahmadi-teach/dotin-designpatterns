﻿using System.Collections.Generic;

namespace TemplateMethodSample
{
    public class CsvDisplay : FileDisplay
    {
        protected override List<KeyValuePair<string, string>> Parse(string content)
        {
            return new List<KeyValuePair<string, string>>();
        }
    }

    public class XmlDisplay : FileDisplay
    {
        protected override List<KeyValuePair<string, string>> Parse(string content)
        {
            return new List<KeyValuePair<string, string>>();
        }
    }
}