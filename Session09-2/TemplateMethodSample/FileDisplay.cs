﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TemplateMethodSample
{
    public abstract class FileDisplay
    {
        public void Display(string path)
        {
            GuardAgainstInvalidFile(path);
            var content = ReadContents(path);
            content = ChangeContentAfterRead(content);
            var parsedContent = Parse(content);
            DisplayContent(parsedContent);
        }
        protected string ChangeContentAfterRead(string content)
        {
            return content;
        }
        protected virtual void DisplayContent(List<KeyValuePair<string, string>> parsedContent)
        {
            parsedContent.ForEach(a =>
            {
                Console.WriteLine($"{a.Key} : {a.Value}");
                Console.WriteLine("--------------------------------");
            });
        }
        protected abstract List<KeyValuePair<string, string>> Parse(string content);
        private string ReadContents(string path)
        {
            return File.ReadAllText(path);
        }
        private void GuardAgainstInvalidFile(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException();
        }
    }
}