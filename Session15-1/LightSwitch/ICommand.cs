﻿namespace LightSwitch
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}