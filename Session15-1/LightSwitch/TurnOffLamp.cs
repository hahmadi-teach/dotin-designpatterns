﻿namespace LightSwitch
{
    public class TurnOffLamp : ICommand
    {
        private readonly Lamp _lamp;
        public TurnOffLamp(Lamp lamp)
        {
            _lamp = lamp;
        }
        public void Execute()
        {
            _lamp.TurnOff();    
        }

        public void Undo()
        {
            _lamp.TurnOn();
        }
    }
}