﻿namespace LightSwitch
{
    public class Lamp       //Receiver
    {
        public bool IsOn { get; private set; }
        public void TurnOn()
        {
            this.IsOn = true;
        }
        public void TurnOff()
        {
            this.IsOn = false;
        }
    }
}