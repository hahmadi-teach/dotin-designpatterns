﻿using System.Collections.Generic;

namespace LightSwitch
{
    public class CompositeCommand : ICommand
    {
        private List<ICommand> _commands;
        public CompositeCommand(List<ICommand> commands)
        {
            _commands = commands;
        }
        public void Execute()
        {
            _commands.ForEach(a=> a.Execute());
        }
        public void Undo()
        {
            _commands.ForEach(a=> a.Undo());
        }
    }
}