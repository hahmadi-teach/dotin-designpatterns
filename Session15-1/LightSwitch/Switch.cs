﻿using System.Collections.Generic;
using System.Linq;

namespace LightSwitch
{
    public class Switch
    {
        private Stack<ICommand> _commands = new Stack<ICommand>();
        public void Perform(ICommand command)
        {
            command.Execute();
            _commands.Push(command);
        }
        public void Undo()
        {
            if (!_commands.Any()) return;
            var commandToUndo = _commands.Pop();
            commandToUndo.Undo();
        }
    }
}