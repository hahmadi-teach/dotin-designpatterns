﻿using System;

namespace LightSwitch
{
    class Program
    {
        static void Main(string[] args)
        {
            var lamp = new Lamp();
            var turnOff = new TurnOffLamp(lamp);
            var turnOn = new TurnOnLamp(lamp);
            
            var lampSwitch = new Switch();
            lampSwitch.Perform(turnOn);
            Console.WriteLine(lamp.IsOn);
            lampSwitch.Undo();
            Console.WriteLine(lamp.IsOn);

            Console.ReadLine();
        }
    }
}
