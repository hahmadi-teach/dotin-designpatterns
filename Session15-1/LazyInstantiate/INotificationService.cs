﻿namespace LazyInstantiate
{
    public interface INotificationService
    {
        void SendWelcome(string username);
    }
}