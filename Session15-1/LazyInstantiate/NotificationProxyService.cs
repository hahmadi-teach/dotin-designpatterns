﻿using System;

namespace LazyInstantiate
{
    public class NotificationProxyService : INotificationService
    {
        private readonly Lazy<INotificationService> _notificationService;
        public NotificationProxyService(Lazy<INotificationService> notificationService)
        {
            _notificationService = notificationService;
        }
        public void SendWelcome(string username)
        {
            _notificationService.Value.SendWelcome(username);
        }
    }
}