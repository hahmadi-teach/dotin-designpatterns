﻿using System;

namespace LazyInstantiate
{
    public class ExpensiveNotificationService : INotificationService
    {
        public ExpensiveNotificationService()
        {
            Console.WriteLine("Expensive Notification Service instantiated !");
        }

        public void SendWelcome(string username)
        {
            Console.WriteLine("Send welcome from expensive notification service");
        }
    }
}