﻿using System;
using Chat.Model;

namespace Chat
{
    class Program
    {
        static void Main(string[] args)
        {
            var chat = new StandardChat();

            var jack = new User("jack");
            var joe  = new User("joe");
            var sarah = new User("sarah");

            chat.Join(jack);
            chat.Join(joe);
            chat.Join(sarah);

            jack.Broadcast("hi !");

            Console.ReadLine();
        }
    }
}
