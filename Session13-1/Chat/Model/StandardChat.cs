﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat.Model
{
    public class StandardChat : IChatHub
    {
        private List<User> _users = new List<User>();
        public void Send(User sender, string receiver, string message)
        {
            var receiverUser =
                this._users.FirstOrDefault(a => a.Username.Equals(receiver, StringComparison.OrdinalIgnoreCase));
            message = PrepareMessage(sender, message);
            receiverUser?.Receive(message);
        }
        public void Broadcast(User sender, string message)
        {
            var allUsers = this._users.Where(a => a != sender).ToList();
            message = PrepareMessage(sender, message);
            allUsers.ForEach(a=> a.Receive(message));
        }
        private static string PrepareMessage(User sender, string message)
        {
            return $"{sender.Username} : {message}";
        }
        public void Join(User user)
        {
            this._users.Add(user);
            user.JoinChat(this);
        }
    }
}
