﻿namespace Chat.Model
{
    public interface IChatHub
    {
        void Send(User sender, string receiver, string message);
        void Broadcast(User sender, string message);
        void Join(User user);
    }
}