﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Model
{
    public class User
    {
        private IChatHub _hub;
        public string Username { get; private set; }
        public User(string username)
        {
            this.Username = username;
        }
        public void Send(string receiver, string message)
        {
            _hub.Send(this, receiver, message);
        }

        public void Broadcast(string message)
        {
            _hub.Broadcast(this,message);
        }
        public void Receive(string message)
        {
            Console.WriteLine($"I'm {Username} and i Received : {message}");
        }
        public void JoinChat(IChatHub hub)
        {
            this._hub = hub;
        }
    }
}
