﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrototypeSample.Model
{
    public abstract class WorkflowItem : ICloneable
    {
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class Condition : WorkflowItem
    {
        private string _statement;
        public Condition(string statement)
        {
            _statement = statement;
        }
    }
}
