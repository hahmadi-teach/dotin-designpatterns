﻿using System;
using ApplicationServices;
using ApplicationServices.Contracts;
using Autofac;

namespace Config
{
    public class WeatherModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherForecastService>()
                .As<IWeatherForecastService>()
                .InstancePerLifetimeScope();

        }
    }
}
