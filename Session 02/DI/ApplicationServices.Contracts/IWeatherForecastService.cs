﻿using System.Collections.Generic;

namespace ApplicationServices.Contracts
{
    public interface IWeatherForecastService
    {
        List<WeatherForecast> GetForecastSummaries();
    }
}