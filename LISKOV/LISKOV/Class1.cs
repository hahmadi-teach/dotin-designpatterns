﻿using System;

namespace LISKOV
{
    public static class Program 
    {
        public static void Main(string[] args)
        {
            //IShapeFactory<Shape> shapeFactory = new CircleFactory();

            Action<Circle> printCircle = Print;
        }

        public static void Print(Shape shape)
        {

        }
    }
    

    public class Shape { }
    public class Circle : Shape {}
    public interface IShapeFactory<out T>
    {
        T Create();
    }

    public class ShapeFactory : IShapeFactory<Shape>
    {
        public Shape Create()
        {
            return new Shape();
        }
    }

    public class CircleFactory : IShapeFactory<Circle>
    {
        public Circle Create()
        {
            return new Circle();
        }
    }
}
