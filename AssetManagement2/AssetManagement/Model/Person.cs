﻿using System.Collections.Generic;
using AssetManagement.Model.Services;

namespace AssetManagement.Model
{
    public class Person
    {
        public List<Asset> Assets { get; set; }
        public Person()
        {
            this.Assets = new List<Asset>();
        }
        public void AcceptVisitor(IVisitor visitor)
        {
            foreach (var asset in this.Assets)
                asset.AcceptVisitor(visitor);
        }
    }
}
