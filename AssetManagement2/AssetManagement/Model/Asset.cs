﻿using AssetManagement.Model.Services;

namespace AssetManagement.Model
{
    public abstract class Asset
    {
        //you can also use dynamic and remove the implementation in concrete classes
        public abstract void AcceptVisitor(IVisitor visitor);       
    }
}