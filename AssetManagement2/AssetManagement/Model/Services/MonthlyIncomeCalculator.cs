﻿namespace AssetManagement.Model.Services
{
    public class MonthlyIncomeCalculator : IVisitor
    {
        private int _monthlyIncome;
        public void Visit(RealEstate realEstate)
        {
            this._monthlyIncome -= realEstate.MonthlyTax;
            this._monthlyIncome += realEstate.MonthlyIncomeRent;
        }

        public void Visit(Car car)
        {
            this._monthlyIncome -= car.MonthlyCost;
        }

        public void Visit(BankAccount account)
        {
            this._monthlyIncome += account.MonthlyInterest;
        }
        public int GetMonthlyIncome() => _monthlyIncome;
    }
}