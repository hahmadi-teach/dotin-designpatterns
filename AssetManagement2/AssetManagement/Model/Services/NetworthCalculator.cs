﻿namespace AssetManagement.Model.Services
{
    public class NetWorthCalculator : IVisitor
    {
        private int _netWorth;
        public void Visit(RealEstate realEstate)
        {
            this._netWorth += realEstate.Price;
        }
        public void Visit(Car car)
        {
            this._netWorth += car.Price;
        }
        public void Visit(BankAccount account)
        {
            this._netWorth += account.Balance;
            this._netWorth -= account.Loan;
        }
        public int GetNetWorth() => _netWorth;
    }

}