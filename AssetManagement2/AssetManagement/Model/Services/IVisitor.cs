﻿namespace AssetManagement.Model.Services
{
    public interface IVisitor
    {
        void Visit(RealEstate realEstate);
        void Visit(Car car);
        void Visit(BankAccount account);
    }
}