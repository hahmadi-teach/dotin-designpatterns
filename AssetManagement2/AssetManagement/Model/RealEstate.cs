﻿using AssetManagement.Model.Services;

namespace AssetManagement.Model
{
    public class RealEstate : Asset
    {
        public int Price { get; set; }
        public int MonthlyTax { get; set; }
        public int MonthlyIncomeRent { get; set; }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}