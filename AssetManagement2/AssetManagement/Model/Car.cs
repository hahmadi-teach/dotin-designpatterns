﻿using AssetManagement.Model.Services;

namespace AssetManagement.Model
{
    public class Car : Asset
    {
        public int Price { get; set; }
        public  int MonthlyCost { get; set; }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}