using System;
using System.Collections.Generic;
using AssetManagement.Model;
using AssetManagement.Model.Services;
using FluentAssertions;
using Xunit;

namespace AssetManagement.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void calculate_netWorth()
        {
            var person = new Person();
            person.Assets.Add(new RealEstate()
            {
                Price = 1000000000,
                MonthlyIncomeRent = 1500000,
                MonthlyTax = 200000
            });
            person.Assets.Add(new Car()
            {
                Price = 100000000,
                MonthlyCost = 250000
            });
            person.Assets.Add(new BankAccount()
            {
                Balance = 500000000,
                Loan = 40000000,
                MonthlyInterest = 2500000
            });

            var calculator = new NetWorthCalculator();
            person.AcceptVisitor(calculator);

            var result = calculator.GetNetWorth();

            result.Should().Be(1560000000);
        }

        [Fact]
        public void calculate_monthly_income()
        {
            var person = new Person();
            person.Assets.Add(new RealEstate()
            {
                Price = 1000000000,
                MonthlyIncomeRent = 1500000,
                MonthlyTax = 200000
            });
            person.Assets.Add(new Car()
            {
                Price = 100000000,
                MonthlyCost = 250000
            });
            person.Assets.Add(new BankAccount()
            {
                Balance = 500000000,
                Loan = 40000000,
                MonthlyInterest = 2500000
            });

            var calculator = new MonthlyIncomeCalculator();
            person.AcceptVisitor(calculator);

            var result = calculator.GetMonthlyIncome();

            result.Should().Be(3550000);
        }
    }
}
