﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net.Sockets;

namespace Problem
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new Logger();

            Console.WriteLine("Do you want separators?");
            var value = Console.ReadLine();
            if (value == "y")
            {
                logger = new SeparatorLogger(logger);
            }
            Console.WriteLine("Do you want colors?");
            value = Console.ReadLine();
            if (value == "y")
            {
                logger = new ColorfulLogger(logger);
            }


            logger.Write("test");

            Console.ReadLine();

        }
    }


}
