﻿using System;

namespace Problem
{
    //Favor Composition over inheritance

    public interface ILogger
    {
        void Write(string message);
    }

    public class Logger : ILogger
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
    public class SeparatorLogger : ILogger
    {
        private readonly ILogger _logger;
        public SeparatorLogger(ILogger logger)
        {
            _logger = logger;
        }
        public void Write(string message)
        {
            Console.WriteLine("----------------");
            _logger.Write(message);
            Console.WriteLine("----------------");
        }
    }

    public class ColorfulLogger : ILogger
    {
        private ILogger _logger;
        public ColorfulLogger(ILogger logger)
        {
            this._logger = logger;
        }

        public void Write(string message)
        {
            var oldColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            _logger.Write(message);
            Console.ForegroundColor = oldColor;
        }
    }


    //public class Logger
    //{
    //    public virtual void Write(string message)
    //    {
    //        Console.WriteLine(message);
    //    }
    //}
    //public class TimeAwareLogger : Logger
    //{
    //    public override void Write(string message)
    //    {
    //        Console.WriteLine($"{DateTime.Now} {message}");
    //    }
    //}
    //public class SeparatorLogger : TimeAwareLogger
    //{
    //    public override void Write(string message)
    //    {
    //        Console.WriteLine("------------------");
    //        base.Write(message);
    //        Console.WriteLine("------------------");
    //    }
    //}
    //public class ColorfulLogger : TimeAwareLogger
    //{
    //    public override void Write(string message)
    //    {
    //        var oldColor = Console.ForegroundColor;
    //        Console.ForegroundColor = ConsoleColor.Yellow;
    //        base.Write(message);
    //        Console.ForegroundColor = oldColor;
    //    }
    //}
}