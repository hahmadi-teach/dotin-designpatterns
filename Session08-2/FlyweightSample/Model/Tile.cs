﻿using System.Drawing;
using System.Windows.Forms;

namespace FlyweightSample.Model
{
    public abstract class Tile
    {
        public Color Color { get; protected set; }
        public void Render(Form form, int x, int y, int width, int height)
        {
            using (var myBrush = new SolidBrush(this.Color))
            {
                var formGraphics= form.CreateGraphics();
                formGraphics.FillRectangle(myBrush, new Rectangle(x, y, width, height));
            }
        }
    }

    public class Grass : Tile
    {
        public Grass()
        {
            this.Color = Color.ForestGreen;
        }
    }
    public class Blood : Tile
    {
        public Blood()
        {
            this.Color = Color.DarkRed;
        }
    }

}