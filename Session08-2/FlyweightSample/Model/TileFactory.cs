﻿using System.Collections.Concurrent;

namespace FlyweightSample.Model
{
    public static class TileFactory
    {
        private static ConcurrentDictionary<string, Tile> _sharedTiles = new ConcurrentDictionary<string, Tile>();
        public static Tile CreateGrass()
        {
            return _sharedTiles.GetOrAdd("GRASS", new Grass());
        }
        public static Tile CreateBlood()
        {
            return _sharedTiles.GetOrAdd("BLOOD", new Blood());
        }
    }
}