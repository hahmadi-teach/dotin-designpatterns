﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlyweightSample.Model;

namespace FlyweightSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < 50; i++)
            {
                var tile = TileFactory.CreateGrass();
                var x = random.Next(0, this.Width);
                var y = random.Next(0, this.Height);
                var width = random.Next(1, 100);
                var height = random.Next(1, 100);
                tile.Render(this, x, y, width, height);
            }

            for (int i = 0; i < 50; i++)
            {
                var tile = TileFactory.CreateBlood();
                var x = random.Next(0, this.Width);
                var y = random.Next(0, this.Height);
                var width = random.Next(1, 100);
                var height = random.Next(1, 100);
                tile.Render(this, x, y, width, height);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
