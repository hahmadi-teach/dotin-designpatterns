﻿using System;

namespace StateSample.States
{
    public abstract class OrderState
    {
        public virtual bool CanUpdate() => false;
        public virtual bool CanShip() => false;
        public virtual OrderState GoToShippedState() => throw new NotSupportedException();
    }
}