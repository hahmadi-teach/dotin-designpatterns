﻿using System;
using System.Collections.Concurrent;

namespace StateSample.States
{
    public static class OrderStateFactory
    {
        private static ConcurrentDictionary<string,OrderState> _states = new ConcurrentDictionary<string, OrderState>();
        public static T Create<T>() where T : OrderState, new()
        {
            return _states.GetOrAdd(typeof(T).Name, a => new T()) as T;
        }
    }
}