﻿namespace StateSample.States
{
    public class ConfirmedState : OrderState
    {
        public override bool CanShip()
        {
            return true;
        }
        public override OrderState GoToShippedState() => OrderStateFactory.Create<ShippedState>();
    }
}