﻿namespace AdapterSample
{
    public class OtherAdapter : ThirdParty, ICacheManager
    {
        public void Add(string key, object value)
        {
            base.AddToCache(key, value);
        }

        public T Get<T>(string key)
        {
            return (T)base.Get(key);
        }
    }

    public class ThirdParty //adaptee
    {
        public void AddToCache(string key, object value)
        {

        }

        public object Get(string key)
        {
            return null;
        }
    }
}