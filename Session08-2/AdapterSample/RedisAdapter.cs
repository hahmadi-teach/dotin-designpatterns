﻿using StackExchange.Redis;

namespace AdapterSample
{
    public class RedisAdapter : ICacheManager
    {
        private StackExchange.Redis.IDatabase _db = null;       //Adaptee
        public void Add(string key, object value)
        {
            //_db.StringSet(key, new RedisValue(value.ToString()));
        }

        public T Get<T>(string key)
        {
            //return _db.StringGet(key) as T;
            return default(T);
        }
    }
}