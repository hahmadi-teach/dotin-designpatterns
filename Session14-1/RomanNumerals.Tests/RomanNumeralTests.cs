using System;
using FluentAssertions;
using RomanNumerals.Parsers;
using Xunit;

namespace RomanNumerals.Tests
{
    public class RomanNumeralTests
    {
        [Theory]
        [InlineData("I",1)]
        [InlineData("II",2)]
        [InlineData("III",3)]
        [InlineData("IV",4)]
        [InlineData("V",5)]
        [InlineData("VI",6)]
        [InlineData("VII",7)]
        [InlineData("VIII", 8)]
        [InlineData("IX", 9)]
        public void converts_units(string input, int expected)
        {
            var result = RomanNumeralParser.Parse(input);

            result.Should().Be(expected);
        }

        [Theory]
        [InlineData("X", 10)]
        [InlineData("XXV", 25)]
        [InlineData("L", 50)]
        [InlineData("LXXXII", 82)]
        [InlineData("XCIX", 99)]
        public void converts_tens(string input, int expected)
        {
            var result = RomanNumeralParser.Parse(input);

            result.Should().Be(expected);
        }


        [Theory]
        [InlineData("DCCXXVI", 726)]
        public void converts_hundreds(string input, int expected)
        {
            var result = RomanNumeralParser.Parse(input);

            result.Should().Be(expected);
        }

        // Reverse Polish Notation
        // SQL

        // 10kg to g      10cm to m           1C to F
    }
}
