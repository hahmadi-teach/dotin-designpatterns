﻿namespace RomanNumerals.Parsers
{
    public class ParsingContext
    {
        public string Input { get; set; }
        public int Result { get; set; }
    }
}