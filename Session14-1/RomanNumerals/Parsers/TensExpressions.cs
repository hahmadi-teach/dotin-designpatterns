﻿namespace RomanNumerals.Parsers
{
    public class TensExpressions : Expression
    {
        protected override string One() => "X";
        protected override string Four() => "L";
        protected override string Five() => "V";
        protected override string Nine() => "XC";
        protected override int Multiplier() => 10;
    }
}