﻿using System.Collections.Generic;
using Microsoft.VisualBasic;

namespace RomanNumerals.Parsers
{
    public static class RomanNumeralParser
    {
        public static int Parse(string input)
        {
            var context = new ParsingContext();
            context.Input = input;
            var expressions = new List<Expression>()
            {
                new HundredExpression(),
                new TensExpressions(),
                new UnitExpression()
            };

            foreach (var expression in expressions)
            {
                expression.Interpret(context);
            }

            return context.Result;
        }
    }
}