﻿namespace RomanNumerals.Parsers
{
    public abstract class Expression
    {
        public void Interpret(ParsingContext context)
        {
            if (context.Input.StartsWith(Four()))
            {
                context.Result += 4 * Multiplier();
                context.Input = context.Input.Remove(0, 2);
            }
            else if (context.Input.StartsWith(Nine()))
            {
                context.Result += 9 * Multiplier();
                context.Input = context.Input.Remove(0, 2);
            }
            else if (context.Input.StartsWith(Five()))
            {
                context.Result += 5 * Multiplier();
                context.Input = context.Input.Remove(0, 1);
            }

            while (context.Input.StartsWith(One()))
            {
                context.Result += 1 * Multiplier();
                context.Input = context.Input.Remove(0, 1);
            }
        }

        protected abstract string One();
        protected abstract string Four();
        protected abstract string Five();
        protected abstract string Nine();
        protected abstract int Multiplier();
    }
}