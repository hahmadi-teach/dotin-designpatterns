﻿using System;

namespace InterpreterSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Interpreter.Parse("ADD (ADD 10 30) 20");

            Console.ReadLine();
        }
    }
}
