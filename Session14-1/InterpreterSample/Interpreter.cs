﻿namespace InterpreterSample
{
    public static class Interpreter
    {
        public static int Parse(string command)
        {
            var expression = ExpressionFactory.Create(command);
            return expression.Interpret();
        }
    }

    public static class ExpressionFactory
    {
        public static Expression Create(string command)
        {
            if (command.StartsWith("ADD"))
            {
                command = command.Remove(0,3);
                return new AddExpression(command);
            }
            return null;
        }
    }
}