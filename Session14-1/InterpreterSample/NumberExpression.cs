﻿namespace InterpreterSample
{
    public class NumberExpression : Expression      //ADD [ ] [ ]
    {
        private readonly string _input;
        public NumberExpression(string input)
        {
            _input = input.Trim();
        }

        public override int Interpret()
        {
            if (string.IsNullOrEmpty(_input)) return 0;
            return int.Parse(_input);
        }
    }
}