﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample
{
    public abstract class Expression
    {
        public abstract int Interpret();
    }
}
