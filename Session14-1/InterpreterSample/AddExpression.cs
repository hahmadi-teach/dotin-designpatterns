﻿using System.Runtime.InteropServices.ComTypes;

namespace InterpreterSample
{
    public class AddExpression : Expression
    {
        private Expression _left;
        private Expression _right;
        public AddExpression(string input)
        {
            input = input.Trim();
            if (input.StartsWith("("))
            {
                _left = ExpressionFactory.Create(input.Substring(1, input.IndexOf(")") - 1));
                _right = new NumberExpression(input.Substring(input.IndexOf(")") + 1, input.Length - input.IndexOf(")") -1));
            }           // ADD (ADD 10 30) 40
            else
            {
                var splitted = input.Split(" ");
                _left = new NumberExpression(splitted[0]);
                _right = new NumberExpression(splitted[1]);
            }
        }
        public override int Interpret()
        {
            return _left.Interpret() + _right.Interpret();
        }
    }
}