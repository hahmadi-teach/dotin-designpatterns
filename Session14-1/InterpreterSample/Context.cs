﻿namespace InterpreterSample
{
    public class Context
    {
        public string Command { get; set; }
        public int Result { get; set; }
    }
}