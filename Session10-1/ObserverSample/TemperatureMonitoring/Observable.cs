﻿using System;
using System.Collections.Generic;

namespace ObserverSample.TemperatureMonitoring
{
    public abstract class Observable<T> : IObservable<T>
    {
        private List<IObserver<T>> _observers = new List<IObserver<T>>();
        public virtual IDisposable Subscribe(IObserver<T> observer)
        {
            this._observers.Add(observer);
            return new Subscription(() => this._observers.Remove(observer));
        }
        public virtual void NotifyAllObservers(T value)
        {
            this._observers.ForEach(a=> a.OnNext(value));
        }
    }
}