﻿using System;

namespace ObserverSample.TemperatureMonitoring
{
    public class Subscription : IDisposable
    {
        private readonly Action _unsubscribeMethod;
        public Subscription(Action unsubscribeMethod)
        {
            _unsubscribeMethod = unsubscribeMethod;
        }
        public void Dispose()
        {
            _unsubscribeMethod.Invoke();
        }
    }
}