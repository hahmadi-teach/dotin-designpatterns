﻿using System;
using System.Diagnostics;

namespace ObserverSample.TemperatureMonitoring.Observers
{
    public class DebugObserver : IObserver<int>
    {
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(int value)
        {
            Debug.WriteLine($"current temperature : {value}");
        }
    }
}