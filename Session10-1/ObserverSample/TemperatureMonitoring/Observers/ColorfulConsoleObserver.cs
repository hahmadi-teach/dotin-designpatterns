﻿using System;

namespace ObserverSample.TemperatureMonitoring.Observers
{
    public class ColorfulConsoleObserver : IObserver<int>
    {
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(int value)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = value > 30 ? ConsoleColor.Red : ConsoleColor.Cyan;
            Console.WriteLine($"current temperature : {value}");
            Console.ForegroundColor = oldColor;
        }
    }
}