﻿using System;

namespace ObserverSample.TemperatureMonitoring.Observers
{
    public class ConsoleObserver : IObserver<int>
    {
        public void OnNext(int value)
        {
            Console.WriteLine($"current temperature : {value}");
        }
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }
    }
}