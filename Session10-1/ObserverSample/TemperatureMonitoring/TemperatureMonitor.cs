﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ObserverSample.TemperatureMonitoring
{
    public class TemperatureMonitor : Observable<int>
    {
        public void Start()
        {
            var random = new Random();
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var temperature = random.Next(1, 50);
                    NotifyAllObservers(temperature);
                    Thread.Sleep(1000);
                }
            });
        }
        public void Stop()
        {
        }
    }
}