﻿using System;
using System.Threading;
using ObserverSample.TemperatureMonitoring;
using ObserverSample.TemperatureMonitoring.Observers;

namespace ObserverSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var temperatureMonitor = new TemperatureMonitor();
            var colorfulConsoleObserver = new ColorfulConsoleObserver();
            var subscription = temperatureMonitor.Subscribe(colorfulConsoleObserver);

            temperatureMonitor.Start();

            Thread.Sleep(5000);
            subscription.Dispose();


            Console.ReadLine();
        }
    }
}
