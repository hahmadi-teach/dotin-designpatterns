﻿using System;
using System.Collections.Generic;
using System.Linq;
using MementoSample.Model;

namespace MementoSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var history = new Stack<EditorSnapshot>();
            var editor = new TextEditor();

            Console.WriteLine("enter command :");
            Console.WriteLine("1. append");
            Console.WriteLine("2. append line");
            Console.WriteLine("3. save");
            Console.WriteLine("4. load");

            while (true)
            {
                var command = int.Parse(Console.ReadLine());

                if (command == 1) editor.Append(Faker.Lorem.GetFirstWord());
                if (command == 2) editor.AppendLine(Faker.Lorem.Sentence());
                if (command == 3) history.Push(editor.GetSnapshot());
                if (command == 4) editor.Restore(history.Pop());

                Console.WriteLine(editor.Render());
                Console.WriteLine("-------------------");
            }
        }
    }
}
