﻿using System;

namespace MementoSample.Model
{
    public class TextEditor
    {
        //private ....
        private string _text;
        public void Append(string text)
        {
            this._text += text;
        }
        public void AppendLine(string line)
        {
            this._text += Environment.NewLine + line;
        }
        public string Render() => _text;

        public EditorSnapshot GetSnapshot()
        {
            return new EditorSnapshot(this._text);
        }
        public void Restore(EditorSnapshot snapshot)
        {
            this._text = snapshot.Text;
        }
    }
}