﻿namespace MementoSample.Model
{
    public class EditorSnapshot
    {
        public string Text { get; private set; }
        public EditorSnapshot(string text)
        {
            this.Text = text;
        }
    }
}