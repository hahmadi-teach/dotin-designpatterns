﻿using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public class Warrior : WarUnit
    {
        public int CoinsInPocket { get; private set; }
        public Warrior(string name) : base(name)
        {
        }

        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void AssignCoins(int coins)
        {
            this.CoinsInPocket = coins;
        }
    }
}