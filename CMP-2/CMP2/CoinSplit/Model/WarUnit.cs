﻿using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public abstract class WarUnit
    {
        public string Name { get; private set; }
        protected WarUnit(string name)
        {
            Name = name;
        }

        public abstract void AcceptVisitor(IVisitor visitor);
        public abstract void AssignCoins(int coins);
    }
}