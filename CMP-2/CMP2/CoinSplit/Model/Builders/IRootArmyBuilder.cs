﻿namespace CoinSplit.Model.Builders
{
    public interface IRootArmyBuilder
    {
        IArmyBuilder WithRootArmy(string name);
    }
}