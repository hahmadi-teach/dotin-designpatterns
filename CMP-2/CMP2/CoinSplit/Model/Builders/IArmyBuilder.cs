﻿namespace CoinSplit.Model.Builders
{
    public interface IArmyBuilder
    {
        IArmyBuilder WithWarriors(params string[] nameOfWarriors);
        IArmyBuilder WithArmy(string name);
        Army Build();
    }
}