﻿namespace CoinSplit.Model.Builders
{
    public class ArmyBuilder : IRootArmyBuilder, IArmyBuilder
    {
        private Army _rootArmy;
        private Army _currentArmy;
        private ArmyBuilder() { }
        public static IRootArmyBuilder CreateNew()
        {
            return new ArmyBuilder();
        }
        public IArmyBuilder WithRootArmy(string name)
        {
            this._rootArmy = new Army(name);
            this._currentArmy = _rootArmy;
            return this;
        }
        public IArmyBuilder WithWarriors(params string[] nameOfWarriors)
        {
            foreach (var nameOfWarrior in nameOfWarriors)
            {
                var warrior = new Warrior(nameOfWarrior);
                _currentArmy.AddUnit(warrior);
            }
            return this;
        }
        public IArmyBuilder WithArmy(string name)
        {
            var army = new Army(name);
            _currentArmy.AddUnit(army);
            _currentArmy = army;
            return this;
        }
        public Army Build() => _rootArmy;
    }
}