﻿namespace CoinSplit.Model.Visitors
{
    public interface IVisitor
    {
        void Visit(Army army);
        void Visit(Warrior warrior);
    }
}