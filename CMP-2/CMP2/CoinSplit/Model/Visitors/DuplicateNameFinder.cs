﻿using System.Collections.Generic;
using System.Linq;

namespace CoinSplit.Model.Visitors
{
    public class DuplicateNameFinder : IVisitor
    {
        private Dictionary<string, int> _names =  new Dictionary<string, int>();

        public void Visit(Army army)
        {
            foreach (var armyUnit in army.Units)
                armyUnit.AcceptVisitor(this);
        }

        public void Visit(Warrior warrior)
        {
            if (_names.ContainsKey(warrior.Name))
                _names[warrior.Name]++;
            else
                _names.Add(warrior.Name,1);
        }

        public Dictionary<string, int> GetDuplicates()
        {
            return this._names.Where(a => a.Value > 1).ToDictionary(a => a.Key, a => a.Value);
        }
    }
}