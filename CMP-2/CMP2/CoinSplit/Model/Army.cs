﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public class Army : WarUnit
    {
        public List<WarUnit> Units { get; private set; }
        public Army(string name, List<WarUnit> units) : base(name)
        {
            Units = units;
        }
        public Army(string name) : this(name, new List<WarUnit>()) { }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void AssignCoins(int coins)
        {
            var eachShare = coins / Units.Count;
            foreach (var unit in Units)
                unit.AssignCoins(eachShare);
        }
        public void AddUnit(WarUnit unit)
        {
            this.Units.Add(unit);
        }
    }
}