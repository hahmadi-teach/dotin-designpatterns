using System;
using FluentAssertions;
using Xunit;

namespace SimpleBuilder.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var uriBuilder = new UriBuilder
            {
                Host = "localhost", 
                Scheme = "http",
                Port = 5000,
                Fragment = "locations/1"
            };

            uriBuilder.Uri.ToString().Should().Be("http://localhost:5000/#locations/1");
        }
    }
}
