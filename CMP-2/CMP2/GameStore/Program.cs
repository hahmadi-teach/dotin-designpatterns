﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameStore.Framework;
using GameStore.Model;
using GameStore.Model.Spec;

namespace GameStore
{
    class Program
    {
        static void Main(string[] args)
        {
            var games = new VideoGame();
            
            var freeGamesForKids = new PcGame().And(new KidsAppropriateGame()).Or(new FreeGame());

            var result = freeGamesForKids.IsSatisfiedBy(games);

            Console.ReadLine();
        }
    }
}
