﻿namespace GameStore.Model
{
    public enum Platform
    {
        Pc,
        Xbox,
        Ps4,
        Android
    }
}