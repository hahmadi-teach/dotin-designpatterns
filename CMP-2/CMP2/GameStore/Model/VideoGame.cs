﻿using System;
using System.Collections.Generic;

namespace GameStore.Model
{
    public class VideoGame
    {
        public string Name { get; set; }
        public DateTime PublishDate { get; set; }
        public Rating Rate { get; set; }
        public int Price { get; set; }
        public List<Platform> SupportedPlatform { get; set; }
    }
}