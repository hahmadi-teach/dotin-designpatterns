﻿using GameStore.Framework;

namespace GameStore.Model.Spec
{
    public  class KidsAppropriateGame : Specification<VideoGame>
    {
        public override bool IsSatisfiedBy(VideoGame game)
        {
            return game.Rate == Rating.E;
        }
    }
}