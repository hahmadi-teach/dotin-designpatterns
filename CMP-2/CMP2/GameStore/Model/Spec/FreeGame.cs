﻿using GameStore.Framework;

namespace GameStore.Model.Spec
{
    public class FreeGame : Specification<VideoGame>
    {
        public override bool IsSatisfiedBy(VideoGame game)
        {
            return game.Price == 0;
        }
    }
}