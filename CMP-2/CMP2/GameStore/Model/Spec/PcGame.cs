﻿using GameStore.Framework;

namespace GameStore.Model.Spec
{
    public class PcGame : Specification<VideoGame>
    {
        public override bool IsSatisfiedBy(VideoGame entity)
        {
            return entity.SupportedPlatform.Contains(Platform.Pc);
        }
    }
}