﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Framework
{
    public class AndSpecification<T> : Specification<T>
    {
        private ISpecification<T> _left;
        private ISpecification<T> _right;
        public AndSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            _left = left;
            _right = right;
        }

        public override bool IsSatisfiedBy(T entity)
        {
            return _left.IsSatisfiedBy(entity) && 
                   _right.IsSatisfiedBy(entity);
        }
    }
}
