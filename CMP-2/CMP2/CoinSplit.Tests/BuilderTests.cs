﻿using System.Configuration;
using System.Data.Common;
using CoinSplit.Model;
using CoinSplit.Model.Builders;
using FluentAssertions;
using Xunit;

namespace CoinSplit.Tests
{
    public class BuilderTests
    {
        [Fact]
        public void Test1()
        {
            var army = ArmyBuilder.CreateNew()
                .WithRootArmy("Gang of warriors")
                    .WithWarriors("John","Joe")
                    .WithArmy("InnerArmy")
                        .WithWarriors("Billy","Bob")
                .Build();


        }
    }
}