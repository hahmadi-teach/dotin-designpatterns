using System;
using System.Collections.Generic;
using CoinSplit.Model;
using CoinSplit.Model.Builders;
using CoinSplit.Model.Visitors;
using FluentAssertions;
using Xunit;

namespace CoinSplit.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void army_split_coins_between_warriors()
        {
            var john = new Warrior("John");
            var joe = new Warrior("Joe");
            var army = new Army("Gang of Warriors", new List<WarUnit>() { joe, john });
            
            army.AssignCoins(500);

            joe.CoinsInPocket.Should().Be(250);
            john.CoinsInPocket.Should().Be(250);
        }

        [Fact]
        public void army_split_coins_between_warriors_and_inner_armies()
        {
            var john = new Warrior("John");
            var joe = new Warrior("Joe");

            var bob = new Warrior("Bob");
            var billy = new Warrior("Billy");
            var innerArmy = new Army("Inner Army", new List<WarUnit>(){ bob, billy});

            var army = new Army("Gang of Warriors", new List<WarUnit>() { joe, john, innerArmy });

            army.AssignCoins(300);

            joe.CoinsInPocket.Should().Be(100);
            john.CoinsInPocket.Should().Be(100);
            bob.CoinsInPocket.Should().Be(50);
            billy.CoinsInPocket.Should().Be(50);
        }

        [Fact]
        public void duplicate_name_finder()
        {
            var john = new Warrior("John");
            var joe = new Warrior("Joe");

            var bob = new Warrior("Bob");
            var billy = new Warrior("Joe");
            var innerArmy = new Army("Inner Army", new List<WarUnit>() {bob, billy});

            var army = new Army("Gang of Warriors", new List<WarUnit>() {joe, john, innerArmy});

            var duplicateFinder = new DuplicateNameFinder();
            army.AcceptVisitor(duplicateFinder);

            var result = duplicateFinder.GetDuplicates();

            result.Should().ContainKey("Joe").WhichValue.Should().Be(2);
        }
    }
}
