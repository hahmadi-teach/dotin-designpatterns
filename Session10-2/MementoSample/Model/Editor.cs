﻿using System;

namespace MementoSample.Model
{
    public class Editor
    {
        public string Text { get; private set; }
        public void Append(string text)
        {
            this.Text += text;
        }
        public void AppendLine(string line)
        {
            this.Text += Environment.NewLine + line;
        }
        public EditorSnapshot GetSnapshot()
        {
            return new EditorSnapshot(this.Text);
        }
        public void SetSnapshot(EditorSnapshot snapshot)
        {
            this.Text = snapshot.Text;
        }
    }
}