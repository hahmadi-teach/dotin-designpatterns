﻿using System;
using System.Collections.Generic;
using MementoSample.Model;

namespace MementoSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var editor = new Editor();
            var snapshots = new Stack<EditorSnapshot>();

            Console.WriteLine("enter a command : ");
            Console.WriteLine("1. appending a word");
            Console.WriteLine("2. appending a line");
            Console.WriteLine("3. save");
            Console.WriteLine("4. load");

            while (true)
            {
                var command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1: 
                        editor.Append(Faker.Lorem.GetFirstWord());
                        break;
                    case 2:
                        editor.AppendLine(Faker.Lorem.Sentence());
                        break;
                    case 3:
                        snapshots.Push(editor.GetSnapshot());
                        break;
                    case 4:
                        editor.SetSnapshot(snapshots.Pop());
                        break;
                }
                Console.WriteLine(editor.Text);
                Console.WriteLine("-------------------------------");
            }
        }
    }
}
