﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ObserverSample.Framework;

namespace ObserverSample.TemperatureMonitoring
{
    public class TemperatureMonitor : Observable<int>, ITemperatureMonitor
    {
        public void StartMonitoring()
        {
            var random = new Random();
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    try
                    {
                        var temperature = random.Next(15, 45);
                        this.NotifyAllAboutNextValue(temperature);
                        Thread.Sleep(1000);
                    }
                    catch (Exception ex)
                    {
                        this.NotifyAllAboutError(ex);
                    }
                }
            });
        }
    }
}
