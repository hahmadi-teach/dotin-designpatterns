﻿using System;

namespace ObserverSample.TemperatureMonitoring
{
    public interface ITemperatureMonitor : IObservable<int>
    {
    }
}