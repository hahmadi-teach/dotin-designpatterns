﻿using System;

namespace ObserverSample.TemperatureMonitoring.Observers
{
    public class ColorfulConsoleObserver : IObserver<int>
    {
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(int value)
        {
            var oldColor = Console.ForegroundColor;
            if (value <= 30)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"Current temperature : {value}");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Current temperature : {value}");
            }

            Console.ForegroundColor = oldColor;
        }
    }
}