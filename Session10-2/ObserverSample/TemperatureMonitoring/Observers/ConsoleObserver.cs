﻿using System;

namespace ObserverSample.TemperatureMonitoring.Observers
{
    public class ConsoleObserver : IObserver<int>
    {
        public void OnCompleted()
        {
        }
        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }
        public void OnNext(int value)
        {
            Console.WriteLine($"Current temperature : {value}");
        }
    }
}