﻿using System;
using System.Collections.Generic;

namespace ObserverSample.Framework
{
    public abstract class Observable<T> : IObservable<T>
    {
        private readonly List<IObserver<T>> _observers = new List<IObserver<T>>();
        public IDisposable Subscribe(IObserver<T> observer)
        {
            this._observers.Add(observer);
            return new Subscription(() => this._observers.Remove(observer));
        }

        protected void NotifyAllAboutNextValue(T value)
        {
            this._observers.ForEach(a=> a.OnNext(value));
        }
        protected void NotifyAllAboutError(Exception ex)
        {
            this._observers.ForEach(a => a.OnError(ex));
        }
        protected void NotifyAllAboutCompletion()
        {
            this._observers.ForEach(a => a.OnCompleted());
        }
    }
}