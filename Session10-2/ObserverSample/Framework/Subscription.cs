﻿using System;

namespace ObserverSample.Framework
{
    public class Subscription : IDisposable
    {
        private readonly Action _unsubscribeAction;
        public Subscription(Action unsubscribeAction)
        {
            _unsubscribeAction = unsubscribeAction;
        }
        public void Dispose()
        {
            _unsubscribeAction.Invoke();
        }
    }
}