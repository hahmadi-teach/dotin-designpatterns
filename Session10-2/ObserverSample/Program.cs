﻿using System;
using System.Threading;
using ObserverSample.TemperatureMonitoring;
using ObserverSample.TemperatureMonitoring.Observers;

namespace ObserverSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var monitor = new TemperatureMonitor();

            using (monitor.Subscribe(new ColorfulConsoleObserver()))
            {
                monitor.StartMonitoring();
                Thread.Sleep(5000);
            }

            Console.ReadLine();
        }
    }
}
