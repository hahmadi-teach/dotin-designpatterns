﻿using System;
using TcpHandling.Framework;

namespace TcpHandling.Model
{
    public class ConfirmPaymentHandler : Handler<TcpPacket>
    {
        protected override bool CanHandleRequest(TcpPacket input)
        {
            return input.Data == "CONFIRM-PAYMENT";
        }
        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of confirm payment !");
        }
    }
}