﻿using System;
using TcpHandling.Framework;

namespace TcpHandling.Model
{
    public class HeartbeatHandler : Handler<TcpPacket>
    {
        protected override bool CanHandleRequest(TcpPacket input)
        {
            return input.Data == "HEARTBEAT";
        }

        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of heartbeat !");
        }
    }
}