﻿using System;
using TcpHandling.Framework;

namespace TcpHandling.Model
{
    public class StartPaymentHandler : Handler<TcpPacket>
    {
        protected override bool CanHandleRequest(TcpPacket input)
        {
            return input.Data == "START-PAYMENT";
        }
        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of Start payment !");
        }
    }
}