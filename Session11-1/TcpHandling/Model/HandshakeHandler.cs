﻿using System;
using TcpHandling.Framework;

namespace TcpHandling.Model
{
    public class HandshakeHandler : Handler<TcpPacket>
    {
        private bool _isAuthenticated;
        protected override bool CanHandleRequest(TcpPacket input)
        {
            return input.Data == "HANDSHAKE";
        }
        protected override void DoHandle(TcpPacket input)
        {
            Console.WriteLine("Response of handshake !");
            this._isAuthenticated = true;
        }

        protected override void BeforeCallingSuccessor()
        {
            if (!_isAuthenticated) throw new UnauthorizedAccessException();
        }
    }
}