﻿using System;
using TcpHandling.Model;

namespace TcpHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            var handler = TcpHandlerFactory.Create();
            var packet1 = new TcpPacket() { Data = "HANDSHAKE"};
            var packet2 = new TcpPacket() { Data = "CONFIRM-PAYMENT" };

            handler.Handle(packet1);
            handler.Handle(packet2);

            Console.ReadLine();
        }
    }
}
