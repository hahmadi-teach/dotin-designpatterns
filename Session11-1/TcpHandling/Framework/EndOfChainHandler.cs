﻿namespace TcpHandling.Framework
{
    public class EndOfChainHandler<T> : IHandler<T>
    {
        public void Handle(T input)
        {
            throw new EndOfChainException();
        }

        public void SetSuccessor(IHandler<T> successor)
        {
            throw new EndOfChainException();
        }
    }
}