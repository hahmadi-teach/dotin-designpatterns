﻿namespace TcpHandling.Framework
{
    public abstract class Handler<T> : IHandler<T>
    {
        protected IHandler<T> Successor;
        public void Handle(T input)
        {
            if (CanHandleRequest(input))
                DoHandle(input);
            else
            {
                BeforeCallingSuccessor();
                CallNext(input);
            }
        }
        protected virtual bool CanHandleRequest(T input)
        {
            return true;
        }
        protected abstract void DoHandle(T input);
        protected virtual void BeforeCallingSuccessor(){}
        protected virtual void CallNext(T input)
        {
            this.Successor.Handle(input);
        }
        public void SetSuccessor(IHandler<T> successor)
        {
            this.Successor = successor;
        }
    }
}