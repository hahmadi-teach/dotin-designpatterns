﻿namespace TcpHandling.Framework
{
    public interface IHandler<T>
    {
        void Handle(T input);
        void SetSuccessor(IHandler<T> successor);
    }
}