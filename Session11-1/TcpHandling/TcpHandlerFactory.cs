﻿using TcpHandling.Framework;
using TcpHandling.Model;

namespace TcpHandling
{
    public static class TcpHandlerFactory
    {
        public static IHandler<TcpPacket> Create()
        {
           return new ChainBuilder<TcpPacket>()
               .Append<HeartbeatHandler>()
               .Append<HandshakeHandler>()
               .Append<StartPaymentHandler>()
               .Append<ConfirmPaymentHandler>()
               .AppendEndOfChainHandler()
               .Build();
        }
    }
}