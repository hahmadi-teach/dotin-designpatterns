﻿namespace SingletonPattern
{
    public class Logger
    {
        private static object _syncLock = new object();
        private static Logger _current;
        public static Logger Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_syncLock)
                    {
                        if (_current == null)
                            _current = new Logger();
                    } 
                }
                return _current;
            }
        }

        public void Write(string message)
        {

        }
    }
}