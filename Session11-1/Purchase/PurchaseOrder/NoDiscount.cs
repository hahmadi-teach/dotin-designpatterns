﻿namespace Purchase.PurchaseOrder
{
    public class NoDiscount : IDiscountStrategy
    {
        public double ApplyDiscount(int price)
        {
            return price;
        }
    }
}