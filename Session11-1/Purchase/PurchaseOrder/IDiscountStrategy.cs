﻿namespace Purchase.PurchaseOrder
{
    public interface IDiscountStrategy
    {
        double ApplyDiscount(int price);
    }
}