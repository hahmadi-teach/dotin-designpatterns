﻿using System;

namespace Purchase.PurchaseOrder
{
    public class Order
    {
        public int TotalPrice { get; set; }
        private IDiscountStrategy discount;

        public double FinalPrice()
        {
            return discount.ApplyDiscount(TotalPrice);
        }
    }
}