﻿namespace Purchase.PurchaseOrder
{
    public class PercentDiscount : IDiscountStrategy
    {
        private readonly double _percent;
        public PercentDiscount(double percent)
        {
            _percent = percent;
        }
        public double ApplyDiscount(int price)
        {
            var discountAmount = (_percent * price) / 100;
            return price - discountAmount;
        }
    }
}