﻿namespace Purchase.PurchaseOrder
{
    public class AmountDiscount : IDiscountStrategy
    {
        private readonly int _discountAmount;
        public AmountDiscount(int discountAmount)
        {
            _discountAmount = discountAmount;
        }

        public double ApplyDiscount(int price)
        {
            return price - _discountAmount;
        }
    }
}