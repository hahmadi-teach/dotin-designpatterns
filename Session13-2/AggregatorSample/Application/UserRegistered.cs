﻿using AggregatorSample.Framework;

namespace AggregatorSample.Application
{
    public class UserRegistered : IEvent
    {
        public string Username { get; }
        public UserRegistered(string username)
        {
            Username = username;
        }
    }
}