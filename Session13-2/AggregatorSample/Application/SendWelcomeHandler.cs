﻿using System.Windows;
using AggregatorSample.Framework;

namespace AggregatorSample.Application
{
    public class SendWelcomeHandler : IEventHandler<UserRegistered>
    {
        public void Handle(UserRegistered @event)
        {
            MessageBox.Show($"welcome ! {@event.Username}");
        }
    }
}