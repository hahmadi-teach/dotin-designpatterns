﻿namespace AggregatorSample.Framework
{
    public interface IEventListener
    {
        void Subscribe<T>(IEventHandler<T> handler) where T : IEvent;
    }
}