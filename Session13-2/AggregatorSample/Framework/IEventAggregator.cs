﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;

namespace AggregatorSample.Framework
{
    public class EventAggregator : IEventListener, IEventPublisher
    {
        private List<object> _eventHandlers = new List<object>();
        public void Subscribe<T>(IEventHandler<T> handler) where T: IEvent
        {
            _eventHandlers.Add(handler);
        }
        public void Publish<T>(T @event) where T : IEvent
        {
            var candidates = _eventHandlers.OfType<IEventHandler<T>>().ToList();
            foreach (var eventHandler in candidates)
                eventHandler.Handle(@event);
        }
    }
}