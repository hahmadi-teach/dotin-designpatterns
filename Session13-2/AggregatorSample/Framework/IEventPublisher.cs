﻿namespace AggregatorSample.Framework
{
    public interface IEventPublisher
    {
        void Publish<T>(T @event) where T : IEvent;
    }
}