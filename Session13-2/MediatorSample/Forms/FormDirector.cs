﻿using System.Windows.Controls;

namespace MediatorSample.Forms
{
    public class FormDirector
    {
        private readonly TextBox _input;
        private readonly Label _description;
        public FormDirector(TextBox input, Label description)
        {
            _input = input;
            _description = description;
            _input.TextChanged += UpdateLabel;
        }

        private void UpdateLabel(object sender, TextChangedEventArgs e)
        {
            var text = _input.Text.ToString();
            _description.Content = text;
        }
    }
}