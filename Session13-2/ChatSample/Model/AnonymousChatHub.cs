﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatSample.Model
{
    public class AnonymousChatHub : IChatHub
    {
        private List<User> _users = new List<User>();
        public void SendMessage(User sender, string receiverName, string message)
        {
            var receiver = _users.FirstOrDefault(a => a.Username.Equals(receiverName, StringComparison.OrdinalIgnoreCase));
            receiver?.Receive($"Anonymous : {message}");
        }

        public void Broadcast(User sender, string message)
        {
            _users.Where(a => a != sender).ToList()
                .ForEach(a => a.Receive($"Anonymous : {message}"));
        }

        public void Join(User user)
        {
            this._users.Add(user);
            user.JoinTheChat(this);
        }
    }
}