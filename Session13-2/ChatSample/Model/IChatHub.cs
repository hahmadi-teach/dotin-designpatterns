﻿namespace ChatSample.Model
{
    public interface IChatHub
    {
        void SendMessage(User sender, string receiver, string message);
        void Broadcast(User sender, string message);
        void Join(User user);
    }
}