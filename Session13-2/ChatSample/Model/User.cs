﻿using System;

namespace ChatSample.Model
{
    public class User
    {
        private IChatHub _chatHub;
        public string Username { get;private set; }
        public User(string username)
        {
            this.Username = username;
        }
        public void Receive(string message)
        {
            Console.WriteLine($"I'm {this.Username} and i received the message : '{message}'");
        }
        public void Send(string receiver, string message)
        {
            _chatHub.SendMessage(this, receiver, message);
        }
        public void Broadcast(string message)
        {
            _chatHub.Broadcast(this, message);
        }
        public void JoinTheChat(IChatHub standardChatHub)
        {
            _chatHub = standardChatHub;
        }
    }
}