﻿using System;
using ChatSample.Model;

namespace ChatSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var ali = new User("ali");
            var sarah = new User("sarah");
            var jack = new User("jack");

            var hub = new AnonymousChatHub();
            hub.Join(ali);
            hub.Join(sarah);
            hub.Join(jack);

            ali.Broadcast("asl plz");

            Console.ReadLine();
        }
    }
}
