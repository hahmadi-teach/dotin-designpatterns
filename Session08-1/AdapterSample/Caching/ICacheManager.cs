﻿namespace AdapterSample.Caching
{
    public interface ICacheManager
    {
        void Add(string key, object value);

        T Get<T>(string key);
    }
}