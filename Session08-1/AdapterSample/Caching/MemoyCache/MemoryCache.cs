﻿using System;

namespace AdapterSample.Caching.MemoyCache
{
    //Third Party
    public class MemoryCache
    {
        public virtual void AddToCache(string key, object value)
        {

        }
        public virtual object GetFromCache(string key)
        {
            throw new NotImplementedException();
        }
    }

    public class MemoryCacheAdapter : MemoryCache, ICacheManager        //Class Adapter
    {
        public override void AddToCache(string key, object value)
        {
            //change the behavior...
            base.AddToCache(key, value);
        }

        public void Add(string key, object value)
        {
            this.AddToCache(key, value);
        }

        public T Get<T>(string key)
        {
            return (T)GetFromCache(key);
        }
    }
}