﻿using System;

namespace FlyweightSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var name1 = "jack";
            var name2 = "jack";
            var name3 = Console.ReadLine();     //jack
            var name4 = string.Intern(Console.ReadLine()); //jack


            Console.WriteLine(name1 == name2);  //TRUE
            Console.WriteLine(ReferenceEquals(name1, name2)); //TRUE 
            Console.WriteLine(ReferenceEquals(name1, name3)); //FALSE
            Console.WriteLine(ReferenceEquals(name1, name4)); //TRUE

        }
    }
}
