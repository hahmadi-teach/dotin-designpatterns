﻿using System.Drawing;
using System.Windows.Forms;

namespace Game.Model
{
    public abstract class Tile
    {
        public Color Color { get; protected set; }
        protected Tile(Color color)
        {
            Color = color;
        }
        public void Render(int x, int y, int width, int height, Form form)
        {
            using (var myBrush = new SolidBrush(this.Color))
            {
                var formGraphics = form.CreateGraphics();
                formGraphics.FillRectangle(myBrush, new Rectangle(x, y, width, height));
            }
        }
    }
}