﻿using System.Drawing;

namespace Game.Model
{
    public class Grass : Tile
    {
        public Grass() : base(Color.DarkRed)
        {
        }
    }
}