﻿using System.Collections.Concurrent;

namespace Game.Model
{
    public static class TileFactory
    {
        private static ConcurrentDictionary<string, Tile> _tiles = new ConcurrentDictionary<string, Tile>();
        public static Tile CreateGrass()
        {
            return _tiles.GetOrAdd("GRASS", new Grass());
        }
        public static Tile CreateBlood()
        {
            return _tiles.GetOrAdd("BLOOD", new Blood());
        }
    }
}