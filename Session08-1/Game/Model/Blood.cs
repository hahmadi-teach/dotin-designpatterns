﻿using System.Drawing;

namespace Game.Model
{
    public class Blood : Tile
    {
        public Blood() : base(System.Drawing.Color.DarkSlateGray)
        {
        }
    }
}