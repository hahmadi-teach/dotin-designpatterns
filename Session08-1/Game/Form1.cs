﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Model;

namespace Game
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var tiles = new List<Tile>();
            for (int i = 0; i < 50; i++)
            {
                var grass = TileFactory.CreateGrass();
                var x = random.Next(1, this.Width);
                var y = random.Next(1, this.Height);
                var height = random.Next(1, 100);
                var width = random.Next(1, 100);
                grass.Render(x, y, height, width, this);
            }
          
        }
    }
}
