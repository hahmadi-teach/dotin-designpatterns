﻿using System;
using PriceCalculation.Services;

namespace PriceCalculation
{
    class Program
    {
        static void Main(string[] args)
        {
            var discount = new PriceCalculatorBuilder()
                                .ApplyDiscount()
                                .WithPercent(10)
                                .Build();

            var markup = new PriceCalculatorBuilder()
                .ApplyMarkup()
                .WithPercent(10)
                .Build();

            Console.WriteLine(markup.Calculate(1000));
            Console.WriteLine(discount.Calculate(1000));

            Console.ReadLine();
        }
    }
}
