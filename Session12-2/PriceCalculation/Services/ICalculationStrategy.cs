﻿namespace PriceCalculation.Services
{
    internal interface ICalculationStrategy
    {
        decimal Calculate(int price);
    }
    internal class PercentStrategy : ICalculationStrategy
    {
        private decimal _percent;
        public PercentStrategy(decimal percent)
        {
            _percent = percent;
        }

        public decimal Calculate(int price)
        {
            return (_percent * price) / 100;
        }
    }

    internal class AmountStrategy : ICalculationStrategy
    {
        private readonly int _amount;
        public AmountStrategy(int amount)
        {
            _amount = amount;
        }

        public decimal Calculate(int price)
        {
            return _amount;
        }
    }
}