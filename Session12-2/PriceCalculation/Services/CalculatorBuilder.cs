﻿using System;

namespace PriceCalculation.Services
{
    public class PriceCalculatorBuilder
    {
        private Func<IPriceCalculator> _priceCalculatorFactory;
        private ICalculationStrategy _calculationStrategy;
        public PriceCalculatorBuilder()
        {
            _calculationStrategy = new AmountStrategy(0);
        }
        public PriceCalculatorBuilder ApplyDiscount()
        {
            _priceCalculatorFactory= () => new Discount(_calculationStrategy);
            return this;
        }
        public PriceCalculatorBuilder ApplyMarkup()
        {
            _priceCalculatorFactory = () => new Markup(_calculationStrategy);
            return this;
        }

        public PriceCalculatorBuilder WithPercent(decimal percent)
        {
            _calculationStrategy =new PercentStrategy(percent);
            return this;
        }
        public PriceCalculatorBuilder WithAmount(int amount)
        {
            _calculationStrategy = new AmountStrategy(amount);
            return this;
        }
        public IPriceCalculator Build()
        {
            return _priceCalculatorFactory.Invoke();
        }
    }
}