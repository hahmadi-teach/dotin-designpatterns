﻿namespace PriceCalculation.Services
{
    public interface IPriceCalculator
    {
        decimal Calculate(int price);
    }

    internal abstract class PriceCalculator : IPriceCalculator
    {
        private readonly ICalculationStrategy _calculationStrategy;
        public PriceCalculator(ICalculationStrategy calculationStrategy)
        {
            _calculationStrategy = calculationStrategy;
        }
        public decimal Calculate(int price)
        {
            var amount = _calculationStrategy.Calculate(price);
            return Apply(amount, price);
        }
        protected abstract decimal Apply(decimal amount, int price);
    }
    internal class Discount : PriceCalculator
    {
        public Discount(ICalculationStrategy calculationStrategy) : base(calculationStrategy)
        {
        }

        protected override decimal Apply(decimal amount, int price)
        {
            return price - amount;
        }
    }
    internal class Markup : PriceCalculator
    {
        public Markup(ICalculationStrategy calculationStrategy) : base(calculationStrategy)
        {
        }
        protected override decimal Apply(decimal amount, int price)
        {
            return price + amount;
        }
    }
}