﻿using System;

namespace WorkflowDesigner.Model
{
    public class Condition : WorkflowItem
    {
        public string Predicate { get; private set; }
        public Condition(string predicate)
        {
            Predicate = predicate;
        }
        public override object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}