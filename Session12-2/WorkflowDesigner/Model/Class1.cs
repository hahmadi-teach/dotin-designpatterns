﻿using System;

namespace WorkflowDesigner.Model
{
    public abstract class WorkflowItem : ICloneable
    {
        public void Render()
        {
            //....
        }
        public abstract object Clone();
    }

    public class Condition : WorkflowItem
    {
        public string Predicate { get; private set; }
        public Condition(string predicate)
        {
            Predicate = predicate;
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}