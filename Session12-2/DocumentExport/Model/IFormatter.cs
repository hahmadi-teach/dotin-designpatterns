﻿using System.Linq;
using System.Linq.Expressions;

namespace DocumentExport.Model
{
    public interface IFormatter
    {
        string Format(string input);
    }

    public class StandardFormatter : IFormatter
    {
        public string Format(string input)
        {
            return input;
        }
    }
    public class ReverseFormatter : IFormatter
    {
        public string Format(string input)
        {
            return string.Concat(input.Reverse());
        }
    }

    public class UppercaseFormatter : IFormatter
    {
        public string Format(string input)
        {
            return input.ToUpper();
        }
    }
}