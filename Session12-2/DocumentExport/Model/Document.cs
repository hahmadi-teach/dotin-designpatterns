﻿namespace DocumentExport.Model
{
    public abstract class Document
    {
        private readonly IFormatter _formatter;
        protected Document(IFormatter formatter)
        {
            _formatter = formatter;
        }
        public string Text { get; set; }

        public override string ToString()
        {
            return _formatter.Format(this.Text);
        }
    }

    public class Book : Document
    {
        public Book(IFormatter formatter) : base(formatter)
        {
        }
        public string Title { get; set; }
        public string ISBN { get; set; }

       
    }

    public class Magazine : Document
    {
        public Magazine(IFormatter formatter) : base(formatter)
        {
        }
    }

}