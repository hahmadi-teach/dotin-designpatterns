﻿using System;
using DocumentExport.Model;

namespace DocumentExport
{
    class Program
    {
        static void Main(string[] args)
        {
            var document = new Book(new UppercaseFormatter());
            document.Text = "test test";

            Console.WriteLine(document.Text);

            Console.WriteLine("Hello World!");
        }
    }
}
