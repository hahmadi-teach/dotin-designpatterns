﻿using System;
using CommandHandlers.Application;
using CommandHandlers.Framework;

namespace CommandHandlers
{
    class Program
    {
        static void Main(string[] args)
        {
            ICommandBus bus = new TempCommandBus();
            bus.Dispatch(new RegisterUser { Username = "Admin", Password = "123456"});

            Console.ReadLine();
        }
    }
}
