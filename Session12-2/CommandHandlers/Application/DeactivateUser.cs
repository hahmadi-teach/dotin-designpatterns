﻿using CommandHandlers.Framework;

namespace CommandHandlers.Application
{
    public class DeactivateUser : ICommand
    {
        public string Username { get; set; }
        public string Reason { get; set; }
    }
}