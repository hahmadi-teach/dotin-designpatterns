﻿using System;
using CommandHandlers.Framework;

namespace CommandHandlers.Application
{
    public class RegisterUserHandler : ICommandHandler<RegisterUser>
    {
        public void Handle(RegisterUser command)
        {
            Console.WriteLine($"User {command.Username} registered !");
        }
    }
}