﻿using CommandHandlers.Framework;

namespace CommandHandlers.Application
{
    public class RegisterUser : ICommand
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}