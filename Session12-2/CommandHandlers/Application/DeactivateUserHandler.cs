﻿using System;
using CommandHandlers.Framework;

namespace CommandHandlers.Application
{
    public class DeactivateUserHandler : ICommandHandler<DeactivateUser> 
                                         //,ICommandHandler<RegisterUser>\
    {
        public void Handle(DeactivateUser command)
        {
            Console.WriteLine($"User {command.Username} deactivate because {command.Reason}");
        }

        //public void Handle(RegisterUser command)
        //{
        //    throw new NotImplementedException();
        //}
    }
}