﻿namespace CommandHandlers.Framework
{
    public class TransactionalCommandHandlerDecorator<T> : ICommandHandler<T> where T : ICommand
    {
        private ICommandHandler<T> _handler;
        public TransactionalCommandHandlerDecorator(ICommandHandler<T> handler)
        {
            _handler = handler;
        }

        public void Handle(T command)
        {
            //begin transaction
            _handler.Handle(command);
            //commit transaction
        }
    }
}