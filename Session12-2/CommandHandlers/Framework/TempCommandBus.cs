﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using CommandHandlers.Application;

namespace CommandHandlers.Framework
{
    public class TempCommandBus : ICommandBus
    {
        private static List<object> _handlers = new List<object>();
        static TempCommandBus()
        {
            _handlers.Add(new RegisterUserHandler());    
            _handlers.Add(new DeactivateUserHandler());
        }

        public void Dispatch<T>(T command) where T : ICommand
        {
            var candidates = _handlers.OfType<ICommandHandler<T>>().ToList();
            candidates.ForEach(a=> a.Handle(command));
        }
    }
}