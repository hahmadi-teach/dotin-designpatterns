﻿using System;

namespace Composition_01
{
    public interface ILogger
    {
        void Write(LogLevel level, string message);
    }

    public class Logger : ILogger
    {
        public virtual void Write(LogLevel level, string message)
        {
            Console.WriteLine($"[{level}] {message}");
        }
    }

    public abstract class LogDecorator : ILogger
    {
        protected readonly ILogger Logger;
        protected LogDecorator(ILogger logger)
        {
            Logger = logger;
        }
        public abstract void Write(LogLevel level, string message);
    }

    public class TimeAwareDecorator : LogDecorator
    {
        public TimeAwareDecorator(ILogger logger) : base(logger)
        {
        }
        public override void Write(LogLevel level, string message)
        {
            message = $"{DateTime.Now} {message}";
            Logger.Write(level, message);
        }
    }


    public class ColorfulDecorator : LogDecorator
    {
        public ColorfulDecorator(ILogger logger) : base(logger)
        {
        }
        public override void Write(LogLevel level, string message)
        {
            var oldColor = Console.ForegroundColor;
            var newColor = SelectColorBasedOnLevel(level);
            Console.ForegroundColor = newColor;
            Logger.Write(level, message);
            Console.ForegroundColor = oldColor;
        }
        private ConsoleColor SelectColorBasedOnLevel(LogLevel level)
        {
            if (level == LogLevel.Error) return ConsoleColor.Red;
            else return ConsoleColor.Yellow;
        }
    }

    public class SeparatorLogger : LogDecorator
    {
        public SeparatorLogger(ILogger logger) : base(logger)
        {
        }
        public override void Write(LogLevel level, string message)
        {
            Logger.Write(level, message);
            Console.WriteLine("----------------");
        }
    }
}