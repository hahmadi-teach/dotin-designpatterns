﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;

namespace Composition_01
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new Logger();

            Console.WriteLine("Do you want time in logs?");
            var value = Console.ReadLine();
            if (value == "y") logger = new TimeAwareLogger(logger);

            Console.WriteLine("Do you want colors in logs?");
            value = Console.ReadLine();
            if (value == "y") logger = new ColorfulLogger(logger);

            Console.WriteLine("Do you want separation between logs?");
            value = Console.ReadLine();
            if (value == "y") logger = new SeparatorLogger(logger);

            logger.Write(LogLevel.Error, "some error");
            logger.Write(LogLevel.Info, "some info");

            Console.ReadLine();

        }
    }
}
