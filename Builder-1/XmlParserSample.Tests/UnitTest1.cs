using System;
using System.Xml;
using XmlParserSample.Builders;
using Xunit;

namespace XmlParserSample.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void converts_to_html()
        {
            var xml = "<Header>Design Patterns<Header>" +
                      "<Body>" +
                      "<Paragraph>I love object oriented patterns !</Paragraph>"+
                      "</Body>" +
                      "<Footer>" +
                      "<TextBlock>Copyright 2020</TextBlock>" +
                      "</Footer>";

            var expectedHtml = "<html><head><title>Design Patterns</title></head>" +
                       "<body>" +
                       "<p>I love object oriented patterns !</p>" +
                       "</body>" +
                       "<footer><span>Copyright 2020</span></footer>";

            var converter = new XmlConverter(new HtmlBuilder());
            var actualHtml = converter.Convert(xml);
        }

        [Fact]
        public void converts_to_plaintext()
        {
            var xml = "<Header>Design Patterns<Header>" +
                      "<Body>" +
                      "<Paragraph>I love object oriented patterns !</Paragraph>" +
                      "</Body>" +
                      "<Footer>" +
                      "<TextBlock>Copyright 2020</TextBlock>" +
                      "</Footer>";


            var converter = new XmlConverter(new PlainTextBuilder());
            var actualOutput = converter.Convert(xml);
        }
    }
}
