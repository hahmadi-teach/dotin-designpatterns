﻿using System;
using FluentMigrator;

namespace FMSample
{
    public class Class1 : Migration
    {
        public override void Up()
        {
            Create.Table("Products")
                .WithColumn("Id").AsInt32().Identity().Unique()
                .WithColumn("Name").AsString(25).NotNullable();
        }

        public override void Down()
        {
        }
    }
}
