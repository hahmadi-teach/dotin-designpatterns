using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace IteratorSample.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void iterate_even_numbers_in_specified_range()
        {
            var range = new EvenNumbersRange(1,10);
            var iterator = range.GetIterator();
            var expected = new List<int>(){2,4,6,8,10};

            var actual = new List<int>();
            while (iterator.HasNext())
                actual.Add(iterator.Next());

            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void resetting_iterator_moves_the_iteration_to_first_step()
        {
            var range = new EvenNumbersRange(1, 10);
            var iterator = range.GetIterator();
            
            iterator.Next();
            iterator.Next();
            iterator.Reset();

            iterator.Next().Should().Be(2);
        }

        [Fact]
        public void test()
        {
            var range = new OddNumbers(1, 10);
            var expected = new List<int> { 1, 3, 5, 7, 9 };

            var numbersInRange = range.ToList();

            numbersInRange.Should().BeEquivalentTo(expected);
        }
    }
}
