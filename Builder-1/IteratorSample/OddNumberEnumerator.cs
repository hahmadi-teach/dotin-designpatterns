﻿using System.Collections;
using System.Collections.Generic;

namespace IteratorSample
{
    public class OddNumberEnumerator : IEnumerator<int>
    {
        private readonly OddNumbers _numbers;
        private int _current;
        private int end;
        public OddNumberEnumerator(OddNumbers numbers)
        {
            _numbers = numbers;
            Reset();
        }

        public bool MoveNext()
        {
            if (this._current + 2 > this.end) return false;
            this._current += 2;
            return true;
        }

        public void Reset()
        {
            if (_numbers.Start % 2 != 0)
                this._current = _numbers.Start - 2;
            else
                this._current = _numbers.Start - 1;
            end = this._numbers.End;
        }

        public int Current => _current;
        object IEnumerator.Current => Current;
        public void Dispose()
        {
        }
    }
}