﻿using System.Collections;
using System.Collections.Generic;

namespace IteratorSample
{
    public class OddNumbers : IEnumerable<int>
    {
        public int Start { get;private set; }
        public int End { get; private set; }

        public OddNumbers(int start, int end)
        {
            Start = start;
            End = end;
        }

        public IEnumerator<int> GetEnumerator()
        {
            return new OddNumberEnumerator(this);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new OddNumberEnumerator(this);
        }
    }
}