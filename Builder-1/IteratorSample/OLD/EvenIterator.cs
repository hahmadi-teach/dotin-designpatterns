﻿namespace IteratorSample
{
    internal class EvenIterator : IIterator<int>
    {
        private readonly EvenNumbersRange _range;
        private int start = 0;
        private int end = 0;
        public EvenIterator(EvenNumbersRange range)
        {
            _range = range;
            Reset();
        }
        public int Next()
        {
            var output = start + 2;
            start += 2;
            return output;
        }
        public bool HasNext()
        {
            return this.start < this.end;
        }

        public void Reset()
        {
            if (_range.Start % 2 == 0)
                start = _range.Start;
            else
                start = _range.Start - 1;
            end = this._range.End;
        }
    }
}