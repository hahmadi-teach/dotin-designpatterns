﻿namespace IteratorSample
{
    public interface IIterator<out T>
    {
        T Next();
        bool HasNext();
        void Reset();
    }
}