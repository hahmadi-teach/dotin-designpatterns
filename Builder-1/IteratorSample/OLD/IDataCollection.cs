﻿namespace IteratorSample
{
    public interface IDataCollection<out T>
    {
        IIterator<T> GetIterator();
    }
}