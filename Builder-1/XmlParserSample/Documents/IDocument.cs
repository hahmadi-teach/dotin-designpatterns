﻿namespace XmlParserSample.Documents
{
    public interface IDocument
    {
        
    }

    public class HtmlDocument : IDocument
    {
        public string Text { get; private set; }
        public HtmlDocument(string text)
        {
            Text = text;
        }
    }

    public class PlainTextDocument : IDocument
    {
      
    }
}