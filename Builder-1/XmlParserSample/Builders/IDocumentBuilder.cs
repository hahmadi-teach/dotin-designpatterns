﻿using XmlParserSample.Documents;

namespace XmlParserSample.Builders
{
    public interface IDocumentBuilder
    {
        IDocumentBuilder AddHeader(string header);
        IDocumentBuilder AddParagraphToBody(string paragraphText);
        IDocumentBuilder AddFooter(string text);
        IDocument Build();
    }
}