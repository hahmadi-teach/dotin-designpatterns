﻿using System.Text;
using XmlParserSample.Documents;

namespace XmlParserSample.Builders
{
    public class HtmlBuilder : IDocumentBuilder
    {
        private StringBuilder outputHtml = new StringBuilder();
        public IDocumentBuilder AddHeader(string header)
        {
            outputHtml.Append($"<html><title>{header}</title><body>");
            return this;
        }

        public IDocumentBuilder AddParagraphToBody(string paragraphText)
        {
            outputHtml.Append($"<paragraph>{paragraphText}</paragraph>");
            return this;
        }

        public IDocumentBuilder AddFooter(string text)
        {
            outputHtml.Append($"</body><footer><span>{text}</span></footer></html>");
            return this;
        }

        public IDocument Build()
        {
            var html = new HtmlDocument(this.outputHtml.ToString());
            return html;
        }
    }
}