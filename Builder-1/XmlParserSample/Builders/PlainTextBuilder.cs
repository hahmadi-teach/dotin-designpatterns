﻿using System.Text;
using XmlParserSample.Documents;

namespace XmlParserSample.Builders
{
    public class PlainTextBuilder : IDocumentBuilder
    {
        private readonly StringBuilder _outputBuilder = new StringBuilder();
        public IDocumentBuilder AddHeader(string header)
        {
            _outputBuilder.AppendLine($"|\t{header}'\t|");
            _outputBuilder.AppendLine($"|\t\t|");
            _outputBuilder.AppendLine($"-------------------------------");
            return this;
        }

        public IDocumentBuilder AddParagraphToBody(string paragraphText)
        {
            _outputBuilder.AppendLine(paragraphText);
            return this;
        }

        public IDocumentBuilder AddFooter(string text)
        {
            _outputBuilder.AppendLine($"-------------------------------");
            _outputBuilder.AppendLine($"|\t\t|");
            _outputBuilder.AppendLine($"|\t{text}'\t|");
            return this;
        }

        public IDocument Build()
        {
            return new PlainTextDocument(); //TODO:complete this
        }
    }
}