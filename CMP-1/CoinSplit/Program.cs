﻿using System;
using System.Collections.Generic;
using System.Text;
using CoinSplit.Model;
using CoinSplit.Model.Builder;
using CoinSplit.Model.Visitors;

namespace CoinSplit
{
    class Program
    {
        static void Main(string[] args)
        {
            //var jack = new Warrior("Jack");
            //var joe = new Warrior("Joe");

            //var billy = new Warrior("Billy");
            //var bob = new Warrior("Bob");
            //var smith = new Warrior("Smith");
            //var innerArmy = new Army("Inner Army", billy, bob, smith );

            //var mainArmy = new Army("Main Army", jack, joe, innerArmy);

            //var mainArmy = new ArmyBuilder()
            //            .CreateArmy("Main Army")
            //                .WithWarriors("Jack", "Joe")
            //                .CreateInnerArmy("InnerArmy 1")
            //                    .WithWarriors("Billy","Bob","Smith")
            //            .Build();

            var mainArmy = ArmyBuilder.New()
                        .CreateArmy("Main Army")
                            .WithWarriors("Jack","Joe")
                            .CreateInnerArmy("Inner 1")
                                .WithWarriors("Billy", "Bob")
                        .Build();

            var result = new StringBuilder()
                .AppendLine("line 1")
                .AppendLine("line 2")
                .AppendLine("line 3")
                .AppendLine("line 4")
                .ToString();

            var builder = new UriBuilder
            {
                Host = "localhost",
                Scheme = "http",
                Port = 5000,
                Query = "userId=1",
                Fragment = "api"
            };

            Console.WriteLine(builder.Uri);

            Console.ReadLine();
        }
    }
}
