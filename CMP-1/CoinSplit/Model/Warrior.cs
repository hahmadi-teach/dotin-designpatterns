﻿using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public class Warrior : WarUnit
    {
        public int Coins { get; private set; }
        public Warrior(string name) : base(name)
        {
        }

        public override void AssignCoins(int coins)
        {
            this.Coins = coins;
        }
        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
        public override string ToString()
        {
            return $"{Name} - Coins in Pocket : {Coins}";
        }
    }
}