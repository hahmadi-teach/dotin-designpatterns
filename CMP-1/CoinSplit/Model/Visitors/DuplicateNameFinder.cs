﻿using System.Collections.Generic;
using System.Linq;

namespace CoinSplit.Model.Visitors
{
    public class DuplicateNameFinder : IVisitor
    {
        private Dictionary<string, int> _names = new Dictionary<string, int>();
        public void Visit(Army army)
        {
            foreach (var unit in army.Units)
                unit.AcceptVisitor(this);
        }

        public void Visit(Warrior warrior)
        {
            if (_names.ContainsKey(warrior.Name))
                _names[warrior.Name]++;
            else
                _names[warrior.Name] = 1;
        }

        public Dictionary<string, int> GetDuplicatesName()
        {
            return _names.Where(a => a.Value > 1).ToDictionary(a=> a.Key, a=>a.Value);
        }
    }
}