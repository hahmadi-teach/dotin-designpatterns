﻿namespace CoinSplit.Model.Builder
{
    public interface IRootArmyBuilder
    {
        IArmyBuilder CreateArmy(string name);
    }

    public interface IArmyBuilder
    {
        IArmyBuilder CreateInnerArmy(string name);
        IArmyBuilder WithWarriors(params string[] names);
        Army Build();
    }
}