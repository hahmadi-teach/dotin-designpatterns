﻿namespace CoinSplit.Model.Builder
{
    public class ArmyBuilder : IRootArmyBuilder, IArmyBuilder
    {
        private Army _rootArmy;
        private Army _currentArmy;
        private ArmyBuilder() { }
        public static IRootArmyBuilder New()
        {
            return new ArmyBuilder();
        }

        public IArmyBuilder CreateArmy(string name)
        {
            _rootArmy = new Army(name);
            _currentArmy = _rootArmy;
            return this;
        }
        public IArmyBuilder CreateInnerArmy(string name)
        {
            var army = new Army(name);
            _currentArmy.AssignUnit(army);
            _currentArmy = army;
            return this;
        }
        public IArmyBuilder WithWarriors(params string[] names)
        {
            foreach (var name in names)
            {
                var warrior = new Warrior(name);
                _currentArmy.AssignUnit(warrior);
            }
            return this;
        }
        public Army Build() => _rootArmy;
    }
}