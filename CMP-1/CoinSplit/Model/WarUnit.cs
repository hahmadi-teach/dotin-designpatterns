﻿using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public abstract class WarUnit
    {
        public string Name { get; private set; }
        protected WarUnit(string name)
        {
            Name = name;
        }
        public abstract void AssignCoins(int coins);
        public abstract void AcceptVisitor(IVisitor visitor);
    }
}