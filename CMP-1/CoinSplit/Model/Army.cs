﻿using System.Collections.Generic;
using System.Linq;
using CoinSplit.Model.Visitors;

namespace CoinSplit.Model
{
    public class Army : WarUnit
    {
        public List<WarUnit> Units { get; private set; }
        public string Name { get; private set; }
        public Army(string name, params WarUnit[] units) : base(name)
        {
            this.Units = units == null ? new List<WarUnit>() : units.ToList();
        }
        public void AssignUnit(WarUnit unit)
        {
            this.Units.Add(unit);
        }
        public override void AssignCoins(int coins)
        {
            var eachShare = coins / this.Units.Count;
            foreach (var warrior in Units)
                warrior.AssignCoins(eachShare);
        }

        public override void AcceptVisitor(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}