﻿namespace LogicalExpressions.Framework
{
    public interface ISpecification<in T>
    {
        bool IsSatisfiedBy(T entity);
    }
}