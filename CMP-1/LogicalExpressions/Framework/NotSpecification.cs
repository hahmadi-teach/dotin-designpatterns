﻿namespace LogicalExpressions.Framework
{
    public class NotSpecification<T> : Specification<T>
    {
        private readonly ISpecification<T> spec;
        public override bool IsSatisfiedBy(T entity)
        {
            return !spec.IsSatisfiedBy(entity);
        }
    }
}