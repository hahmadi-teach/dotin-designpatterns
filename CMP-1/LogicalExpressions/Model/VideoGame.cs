﻿using System;
using System.Collections.Generic;

namespace LogicalExpressions.Model
{
    public class VideoGame
    {
        public string Name { get; set; }
        public DateTime PublishDate { get; set; }
        public Rating Rating { get; set; }
        public List<Platform> Platforms { get; set; }
        public int Price { get; set; }

        public override string ToString()
        {
            var platforms = string.Join(",", Platforms);
            return $"{Name} - {Price}$ - Available on : {platforms}";
        }
    }
}