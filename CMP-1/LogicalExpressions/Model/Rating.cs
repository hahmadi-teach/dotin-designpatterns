﻿namespace LogicalExpressions.Model
{
    public enum Rating
    {
        E,
        PG13,
        R
    }
}