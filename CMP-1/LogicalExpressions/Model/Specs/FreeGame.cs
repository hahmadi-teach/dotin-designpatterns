﻿using LogicalExpressions.Framework;

namespace LogicalExpressions.Model.Specs
{
    public class FreeGame : Specification<VideoGame>
    {
        public override  bool IsSatisfiedBy(VideoGame entity)
        {
            return entity.Price == 0;
        }
    }
}