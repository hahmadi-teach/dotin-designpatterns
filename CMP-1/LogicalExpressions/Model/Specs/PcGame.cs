﻿using LogicalExpressions.Framework;

namespace LogicalExpressions.Model.Specs
{
    public class PcGame : Specification<VideoGame>
    {
        public override bool IsSatisfiedBy(VideoGame entity)
        {
            return entity.Platforms.Contains(Platform.Pc);
        }
    }
}