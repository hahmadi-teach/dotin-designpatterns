﻿using LogicalExpressions.Framework;

namespace LogicalExpressions.Model.Specs
{
    public class KidsAppropriateGame : Specification<VideoGame>
    {
        public override bool IsSatisfiedBy(VideoGame game)
        {
            return game.Rating != Rating.R;
        }
    }
}