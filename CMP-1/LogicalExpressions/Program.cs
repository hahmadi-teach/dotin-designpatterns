﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using LogicalExpressions.Framework;
using LogicalExpressions.Model;
using LogicalExpressions.Model.Specs;

namespace LogicalExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new VideoGame()
            {
                Name = "Age of empires",
                PublishDate = new DateTime(2000,1,1),
                Platforms = new List<Platform>() {Platform.Pc},
                Price = 1000,
                Rating = Rating.E
            };

            //var validator = new AndSpecification<VideoGame>(new PcGame(), new FreeGame());
            //var validator = new PcGame().And(new FreeGame());
            var validator = new FreeGame().Or(new KidsAppropriateGame()).And(new PcGame());

            var result = validator.IsSatisfiedBy(game);
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
