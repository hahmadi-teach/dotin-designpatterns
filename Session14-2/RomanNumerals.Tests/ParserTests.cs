using System;
using FluentAssertions;
using Xunit;

namespace RomanNumerals.Tests
{
    public class ParserTests
    {
        [Theory]
        [InlineData("I", 1)]
        [InlineData("II", 2)]
        [InlineData("III", 3)]
        [InlineData("IV", 4)]
        [InlineData("V", 5)]
        [InlineData("VI", 6)]
        [InlineData("VII", 7)]
        [InlineData("VIII", 8)]
        [InlineData("IX", 9)]
        public void parse_units(string input, int expected)
        {
            var actual = Parser.Parse(input);

            actual.Should().Be(expected);
        }

        [Theory]
        [InlineData("X", 10)]
        [InlineData("XV", 15)]
        [InlineData("XXI", 21)]
        [InlineData("XL", 40)]
        [InlineData("L", 50)]
        [InlineData("XLV", 45)]
        [InlineData("XLIX", 49)]
        [InlineData("XCIX", 99)]
        public void parse_tens(string input, int expected)
        {
            var actual = Parser.Parse(input);

            actual.Should().Be(expected);
        }

        [Theory]
        [InlineData("C", 100)]
        [InlineData("CL", 150)]
        [InlineData("CDL", 450)]
        [InlineData("DXXXVII", 537)]
        [InlineData("CLXXII", 172)]
        [InlineData("CMXCIX", 999)]
        public void parse_hundreds(string input, int expected)
        {
            var actual = Parser.Parse(input);

            actual.Should().Be(expected);
        }

        [Theory]
        [InlineData("M", 1000)]
        [InlineData("MMXX", 2020)]
        [InlineData("MMMCMXCIX", 3999)]
        public void parse_thousands(string input, int expected)
        {
            var actual = Parser.Parse(input);

            actual.Should().Be(expected);
        }
    }
}
