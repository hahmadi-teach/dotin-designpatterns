using InterpreterSample.Expressions;

namespace InterpreterSample
{
    public class ExpresssionFactory
    {
        public static Expression CreateExpression(string command)
        {
            Expression expression = null;

            if (command.StartsWith("ADD"))
               return new AddExpression(command);

            return new NumberExpression(command);
        }
    }
}