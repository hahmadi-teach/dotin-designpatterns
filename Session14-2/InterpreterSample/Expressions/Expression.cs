﻿namespace InterpreterSample.Expressions
{
    public abstract class Expression
    {
        public abstract int Interpret();
    }
}