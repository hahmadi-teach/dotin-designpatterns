﻿namespace InterpreterSample.Expressions
{
    public class AddExpression : Expression
    {
        private readonly string _arguments;
        public AddExpression(string command)
        {
            _arguments = command.Trim().Remove(0, 3).Trim();
        }

        public override int Interpret()
        {
            Expression _left = null;
            Expression _right = null;
            if (_arguments.StartsWith("("))
            {
                var _leftArgument = _arguments.Substring(1, _arguments.IndexOf(")") -1);
                var _rightArgument = _arguments.Substring(_arguments.IndexOf(")") +1 , _arguments.Length - _arguments.IndexOf(")") -1);
                _left = ExpresssionFactory.CreateExpression(_leftArgument);
                _right = ExpresssionFactory.CreateExpression(_rightArgument);
            }
            else
            {
                var numbers = _arguments.Split(" ");
                _left = ExpresssionFactory.CreateExpression(numbers[0]);
                _right = ExpresssionFactory.CreateExpression(numbers[1]);
            }

            return _left.Interpret() + _right.Interpret();
        }
    }
}