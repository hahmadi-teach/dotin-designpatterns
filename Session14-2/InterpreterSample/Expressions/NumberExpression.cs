﻿namespace InterpreterSample.Expressions
{
    public class NumberExpression : Expression
    {
        private readonly string _input;
        public NumberExpression(string input)
        {
            _input = input.Trim();
        }
        public override int Interpret()
        {
            return int.Parse(_input);
        }
    }
}