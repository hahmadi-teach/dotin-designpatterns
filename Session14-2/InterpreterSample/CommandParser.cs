namespace InterpreterSample
{
    public class CommandParser
    {
        public static int Execute(string command)
        {
            var expression = ExpresssionFactory.CreateExpression(command);

            return expression.Interpret();
        }
    }
}