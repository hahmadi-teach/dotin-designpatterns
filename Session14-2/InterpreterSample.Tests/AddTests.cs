using System;
using FluentAssertions;
using Xunit;

namespace InterpreterSample.Tests
{
    public class AddTests
    {
        [Fact]
        public void add_two_numbers()
        {
            var request = "ADD 10 20";
            var expected = 30;

            var actualResult = CommandParser.Execute(request);

            actualResult.Should().Be(expected);
        }

        [Fact]
        public void adds_number_with_the_result_of_another_add_operation()
        {
            var request = "ADD (ADD 10 30) 20";
            var expected = 60;

            var actualResult = CommandParser.Execute(request);

            actualResult.Should().Be(expected);
        }

    }
}
