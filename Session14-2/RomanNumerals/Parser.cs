﻿using System;
using System.Collections.Generic;

namespace RomanNumerals
{
    public static class Parser
    {
        public static int Parse(string input)
        {
            var context = new InterpretationContext(input);
            var expressions = new List<Expression>()
            {
                new Thousands(),
                new Hundreds(),
                new Tens(),
                new Units()
            };

            foreach (var expression in expressions)
                expression.Interpret(context);

            return context.Result;
        }
    }
}
