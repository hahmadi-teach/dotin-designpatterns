﻿namespace RomanNumerals
{
    public abstract class Expression
    { 
        public abstract void Interpret(InterpretationContext context);
    }
}