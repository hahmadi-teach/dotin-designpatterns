﻿namespace RomanNumerals
{
    public class Tens : RomanExpression
    {
        protected override string One() => "X";
        protected override string Four() => "XL";
        protected override string Five() => "L";
        protected override string Nine() => "XC";
        protected override int Multiplier() => 10;
    }
}