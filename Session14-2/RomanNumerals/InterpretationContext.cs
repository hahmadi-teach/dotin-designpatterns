﻿namespace RomanNumerals
{
    public class InterpretationContext
    {
        public string Input { get; set; }
        public int Result { get; set; }
        public InterpretationContext(string input)
        {
            Input = input;
        }
    }
}