﻿namespace RomanNumerals
{
    public class Hundreds : RomanExpression
    {
        protected override string One() => "C";
        protected override string Four() => "CD";
        protected override string Five() => "D";
        protected override string Nine() => "CM";
        protected override int Multiplier() => 100;
    }
}