﻿namespace RomanNumerals
{
    public class Thousands : Expression
    {
        public override void Interpret(InterpretationContext context)
        {
            while (context.Input.StartsWith("M"))
            {
                context.Result += 1000;
                context.Input = context.Input.Remove(0, 1);
            }
        }
    }
}